# -*- coding: utf-8 -*-
##############################################################################
#
# Author: Bradley C. Dallin
# email: bdallin@wisc.edu
# 
##############################################################################

##############################################################################
# Imports
##############################################################################

from __future__ import print_function, division
import mdtraj as md
import numpy as np

##############################################################################
# FUNTIONS
##############################################################################
def angles( traj, ligand_indices, ref_vector = np.array([ 0, 1 ]), periodic = True ):
    R'''
    Function to calculate the tilt angle of the ligand SAM from provided indices
    '''
    new_traj = traj[:]
    pairs = []
    for indices in ligand_indices:
        ## GET LIGAND COM COORDS AND ASSIGN TO A DUMMY ATOM
        ligand_com = new_traj.xyz[ :, indices, : ].mean(axis=1)
        new_traj.xyz[ :, indices[1], : ] = ligand_com
        pairs.append([ indices[0], indices[1] ])
    
    ## DETERMINE LIGAND VECTORS
    vectors = np.squeeze( md.compute_displacements( new_traj, np.array(pairs), periodic = periodic ) )
    
    ## CALCULATE TILT ANGLE
    dist = np.sqrt( np.sum( vectors**2., axis = 1 ) )
    tilt_angles = np.arccos( np.abs(vectors[:,2] / dist ) ) * 180 / np.pi
    
    ## CALCULATE ORIENTATIONAL ANGLE
    vector_xy = vectors[:,:2]
    dot = vector_xy.dot( ref_vector )
    mag = np.sqrt( np.sum( ref_vector**2. ) ) * np.sqrt( np.sum( vector_xy**2., axis = 1 ) )
    orient_angles = np.arccos( dot / mag ) * 180 / np.pi
    
    return tilt_angles, orient_angles

def rotate_ligands( traj, new_angle, ligand_indices, orient, periodic = True ):
    R'''
    Function to rotate ligands to desired orientation
    '''
    new_traj = traj[:]
    theta = ( orient - new_angle ) * np.pi / 180.
    rot_mat_cw = np.array([ [ np.cos( theta ), np.sin( theta ) ], [ -1.*np.sin( theta ), np.cos( theta ) ] ])
    rot_mat_ccw = np.array([ [ np.cos( theta ), -1.*np.sin( theta ) ], [ np.sin( theta ), np.cos( theta ) ] ])

    for ii, indices in enumerate( ligand_indices ):
        ## DETERMINE LIGAND VECTORS
        x = traj.xyz[0,indices,0]
        x0 = traj.xyz[0,indices[0],0]
        y = traj.xyz[0,indices,1]
        y0 = traj.xyz[0,indices[0],1]
        if new_angle < orient.mean() and x.mean() < x0:
            print( 'rotating clockwise' )
            new_x = ( rot_mat_cw[0,0,ii] * ( x - x0 ) ) + ( rot_mat_cw[0,1,ii] * ( y - y0 ) )
            new_y = ( rot_mat_cw[1,0,ii] * ( x - x0 ) ) + ( rot_mat_cw[1,1,ii] * ( y - y0 ) )
        else:
            print( 'rotating counterclockwise' )
            new_x = ( rot_mat_ccw[0,0,ii] * ( x - x0 ) ) + ( rot_mat_ccw[0,1,ii] * ( y - y0 ) )
            new_y = ( rot_mat_ccw[1,0,ii] * ( x - x0 ) ) + ( rot_mat_ccw[1,1,ii] * ( y - y0 ) )
        
        new_xy = np.vstack(( new_x + x0, new_y + y0 )).transpose()    
        new_traj.xyz[:,indices,:2] = new_xy[np.newaxis,...]
        
    return new_traj

### CLASS DEFINITION: READS GRO FILE
class extract_gro:
    '''
    In this class, it will take the gro file and extract all the details
    Written by: Alex K. Chew (alexkchew@gmail.com)
    INPUTS:
        gro_file_path: full path to gro file
    OUTPUTS:
        ### FROM GRO FILE
        self.ResidueNum: Integer list containing residue numbers
        self.ResidueName: String name containing residue name (SOL, for example)
        self.AtomName: String containing atom name (C1, H2, etc.)
        self.AtomName_nospaces: Same as before iwthout spaces
        self.AtomNum: Integer list containing atom numbers (1, 2, 3, ...)
        self.xCoord: Float list of x-coordinates
        self.yCoord: Float list of y-coordinates
        self.zCoord: Float list of z-coordinates
        self.Box_Dimensions: List containing box dimensions
        
        ### CALCULATED/MODIFIED RESULTS
        self.unique_resid: Unique residue IDs
        self.total_atoms: total atoms
        self.total_residues: total residues
    '''

    ### INITIALIZATION
    def __init__(self, gro_file_path):
        print("\n--- CLASS: extract_gro ---")
        print("*** EXTRACTING GRO FILE: %s ***"%(gro_file_path))
        
        ## SEEING IF THE PATH EVEN EXISTS
        import os.path
        if os.path.isfile(gro_file_path) == False:
            print("ERROR! Gro file does not exists!")
            print("Check gro file path: %s"%(gro_file_path))
            print("Stopping here to prevent further errors!")
            sys.exit()
        
        with open(gro_file_path, "r") as outputfile: # Opens gro file and make it writable
            fileData = outputfile.readlines()
    
        # Deletion of first two rows, simply the title and atom number
        del fileData[0]
        del fileData[0]

        # We know the box length is the last line. Let's extract that first.
        currentBoxDimensions = fileData[-1][:-1].split() # Split by spaces
        self.Box_Dimensions = [ float(x) for x in currentBoxDimensions ] # As floats
    
        # Deleting last line for simplicity
        del fileData[-1]
        
        # Since we know the format of the GRO file to be:
        '''
            5 Character: Residue Number (Integer)
            5 Character: Residue Name (String)
            5 Character: Atom name (String)
            5 Character: Atom number (Integer)
            8 Character: X-coordinate (Float, 3 decimal places)
            8 Character: Y-coordinate (Float, 3 decimal places)
            8 Character: Z-coordinate (Float, 3 decimal places)
        '''
        # We can extract the data for each line according to that format
        self.ResidueNum = []
        self.ResidueName = []
        self.AtomName = []
        self.AtomNum = []
        self.xCoord = []
        self.yCoord = []
        self.zCoord = []
        
        # Using for-loop to input into that specific format
        for currentLine in fileData:
            self.ResidueNum.append( int(currentLine[0:5]) )
            self.ResidueName.append( str(currentLine[5:10]) )
            self.AtomName.append( str(currentLine[10:15]) )
            self.AtomNum.append( int(currentLine[15:20]) )
            self.xCoord.append( float(currentLine[20:28]) )
            self.yCoord.append( float(currentLine[28:36]) )
            self.zCoord.append( float(currentLine[36:44]) )
        
        # CALCULATING TOTAL ATOMS
        self.total_atoms = len(self.AtomNum)
        
        # FINDING UNIQUE RESIDUE ID's
        self.unique_resid = list(set(self.ResidueNum))
        # CALCULATING TOTAL RESIDUES
        self.total_residues = len(self.unique_resid)
        
        
        ## REMOVING SPACES
        self.AtomName = [ atom.replace(" ", "") for atom in self.AtomName]
        self.ResidueName = [ res.replace(" ", "") for res in self.ResidueName]
        
        ### PRINTING SUMMARY
        print("TOTAL ATOMS: %d"%(self.total_atoms) )
        print("TOTAL RESIDUES: %d"%(self.total_residues))
        print("BOX DIMENSIONS: %s"%( [format(x, ".3f") for x in self.Box_Dimensions] ))


### FUNCTION TO PRINT THE GRO FILE
def print_gro_file( output_gro, output_folder, resids, residue_name, atom_name, geom, box_dimension ):
    '''
    The purpose of this script is to write the .gro file necessary to run GMX.
    INPUTS:
        output_gro: [str] 
            Gro file name
        output_folder: [str] 
            Directory to gro file
        resids: [list] 
            Contains list of residue numbers
        residue_name: [list] 
            Contains list of residue names
        atom_name: [list] 
            List containing atom names
        geom: [np.array, shape=(N,3)] 
            Coordinates for ligand system
        box_dimension: [list] 
            box dimensions of your system in nm
    '''
    ## FINDING TOTAL NUMBER OF ATOMS USING GEOMETRY
    num_atoms = len(geom)
    
    ## DEFINING OUTPUT PATH
    output_gro_path = output_folder + '/' + output_gro # Gives full gro path
    
    ## CREATING FILE AND OUTPUTTING
    print("OUTPUTTING GRO FILE %s WITH %d ATOMS"%(output_gro_path, num_atoms))  # Let user know what is being outputted
    
    with open(output_gro_path, "w") as outputfile: # Opens gro file and make it writable
    
        ## WRITING HEADER
        outputfile.write("GRO file developed by print_gro_file tool in MDBuilder\n") # Title for .gro, does not affect anything
        outputfile.write("%d\n"%(num_atoms)) # Total number of atoms, important! Must match topology file
        
        ## DEFINING INITIAL ATOM NUMBER
        atom_num=1; # Start at one
        for bead_index in range(0, num_atoms):
            outputfile.write("%5d%-5s%-5s%5d%8.3f%8.3f%8.3f\n"%(resids[bead_index], residue_name[bead_index], atom_name[bead_index], atom_num, geom[bead_index][0], geom[bead_index][1], geom[bead_index][2]))
            atom_num +=1
            
            # Make sure that atom_num is not greater than 99999, GROMACS cannot read more than 5 digits
            if atom_num > 99999:
                atom_num=0 # Resetting atom number
                
        ## WRITING BOX DIMENSIONS
        outputfile.write("%f %f %f\n"%(box_dimension[0], box_dimension[1], box_dimension[2]))
    return


##############################################################################
# Stand alone script (TESTING)
##############################################################################

if __name__ == "__main__":    
    ## TESTING
    new_angle = 360
    ref = np.array([ 0, 1 ])
    wd = r'R:/simulations/polar_sams/tilt_angle_dependence/sam_single_8x8_janus_300K_C12CONH20.5_dodecanethiol0.5_tip3p_nvt_CHARMM36_2x2x0.3nm/x_axis/'
    gro = 'sam.gro'
    traj = md.load( wd + gro )
    
    ligand_ndx = [ [ atom.index for atom in residue.atoms ] for residue in traj.topology.residues if residue.name not in ['HOH','MET'] ]
        
    tilt, orient = angles( traj, ligand_ndx, ref_vector = ref, periodic = False )
#    print( tilt.mean(), orient.mean() )
    new_traj = rotate_ligands( traj, new_angle, ligand_ndx, orient, periodic = True )
    new_tilt, new_orient = angles( new_traj, ligand_ndx, ref_vector = ref, periodic = True )
    
    print( traj.xyz[0,:,0].size, new_traj.xyz[0,:,0].size )
    print( tilt.mean(), new_tilt.mean() )
    print( orient.mean(), new_orient.mean() )
    
    ## WRITE NEW GRO FILE
    geom = [ [ coord for coord in coords ] for coords in np.squeeze( new_traj.xyz ) ]
    orig_gro = extract_gro( wd + gro )
    print_gro_file( "sam_" + str(new_angle) + ".gro", wd, orig_gro.ResidueNum, orig_gro.ResidueName, orig_gro.AtomName, geom, orig_gro.Box_Dimensions )