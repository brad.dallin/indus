#!/bin/bash

# gather environment variables
sim_name=SIMNAME
pull_rate=PULLRATE
sim_time=SIMULATIONTIME
sim_temperature=SIMULATIONTEMP
forcefield=FORCEFIELD
# Technical flags
num_mpi=NUMMPI  # number of mpi threads
num_omp=NUMOMP  # number of open mp threads
cores_per_window=NUMPERWINDOW
export OMP_NUM_THREADS=$num_omp

# set ensemble to nvt (must be nvt for water with free interface)
ensemble=nvt
bias_const="100.0" # kj/mol/N^2

# source indus enabled gromacs (gmx v2016.6, plumed v2.5.1 w/ indus patch)
source "$HOME/.bashrc"
load_gmx_2016-6_plumed_2-5-1

# set path variables
source $HOME/indus/indus_rc.sh
source ./indus_details.sh
wd="$PWD"

# analysis paths and files
path_analysis="$PATH_PREP_SCRIPTS/analysis_files"
py_position="$path_analysis/indus_slab_position.py"
py_truncate="$path_analysis/truncate_indus_data.py"
py_histogram="$path_analysis/combine_histo.py"
py_analysis="$path_analysis/analyze_indus_wham.py"
wham_analyis="$path_analysis/grossfield_wham/execute_wham_2D"

# Input/Output folders
input_folder=input_files   # includes topology, mdp files, itp files, etc. 
output_folder=output_files # includes final trajectory files, analysis files, etc.

# grossfield wham uses kcal/mol, can be changed to kJ/mol but must be recompiled
# see http://membrane.urmc.rochester.edu/sites/default/files/wham/doc.pdf
# easier to just convert, where PLUMED, GROMACS, and INDUS use kJ/mol
spring_weak_kcal=$(awk "BEGIN{ print 0.239006 * $SPRING_WEAK; exit}")
spring_strong_kcal=$(awk "BEGIN{ print 0.239006 * $SPRING_STRONG; exit}")

# run nvt equil simulation
if [ $RUN_EQUIL -eq 1 ]; then
    echo ""
    echo "Running $ensemble equilibration simulation"
    # ensure we are in the proper directory
    cd $wd/
    echo "  Creating $wd/equil"
    echo "  Copying $wd/$forcefield/ to $wd/$ensemble"
    echo "  Copying $wd/$input_folder/${ensemble}_equil.mdp to $wd/equil/${ensemble}_equil.mdp"
    mkdir -p equil
    cp -r $forcefield/ equil/
    sed -e s/SIMTEMP/$sim_temperature/g $input_folder/${ensemble}_equil_water.mdp > equil/${ensemble}_equil_water.mdp 
    sed -e s/SIMTEMP/$sim_temperature/g $input_folder/${ensemble}_equil.mdp > equil/${ensemble}_equil.mdp

    cp $input_folder/${sim_name}* equil/
    cd equil/

    mpiexec -np 1 gmx_mpi grompp -f ${ensemble}_equil_water.mdp -o ${sim_name}_equil_water.tpr -c ${sim_name}.gro -p ${sim_name}.top
    mpiexec -np $num_mpi gmx_mpi mdrun -ntomp $num_omp -deffnm ${sim_name}_equil_water

    mpiexec -np 1 gmx_mpi grompp -f ${ensemble}_equil.mdp -o ${sim_name}_equil.tpr -c ${sim_name}_equil_water.gro -p ${sim_name}.top
    mpiexec -np $num_mpi gmx_mpi mdrun -ntomp $num_omp -deffnm ${sim_name}_equil

    mpiexec -np 1 gmx_mpi trjconv -f ${sim_name}_equil.xtc -o ${sim_name}_equil_whole.xtc -s ${sim_name}_equil.tpr -pbc whole << INPUT
0
INPUT

    mpiexec -np 1 gmx_mpi trjconv -f ${sim_name}_equil.gro -o ${sim_name}_equil_whole.gro -s ${sim_name}_equil.tpr -pbc whole << INPUT
0
INPUT

    # remove unnecessary files
    rm -f \#*\#
    cd ../
fi

echo ""
echo ""
# begin indus simulation
echo "Beginning INDUS simulation"

# determine placement of slab on SAM surface
if [[ ! -e $wd/equil/${sim_name}_equil.gro && ! -e $wd/equil/${sim_name}_equil.xtc ]]; then
    echo ""
    echo "  ERROR! Equilibration simulation not completed"
    echo ""; exit
else
    if [ $CALC_DIMS -eq 1 ]; then
        python $py_position -f ${sim_name}_equil_whole -w $wd/equil/ -p $wd/$output_folder/ -s "wc_interface" -d $SLAB_DIMENSIONS -N $NUM_WATER
    fi

    slab_position=$(tail -n 1 $wd/$output_folder/${sim_name}_slab_coordinates.csv)
    slab_position=${slab_position##*: }
    x_center=$( echo ${slab_position%%,*} | bc)
    x_center=$(printf "%.3f" $x_center)
    y_center=${slab_position%,*}
    y_center=$( echo ${y_center#*,} | bc)
    y_center=$(printf "%.3f" $y_center)
    z_center=$( echo ${slab_position##*,} | bc)
    z_center=$(printf "%.3f" $z_center)
    echo "  centering INDUS slab at: $slab_position"

    slab_dimensions=$(tail -n 1 $wd/$output_folder/${sim_name}_slab_dimensions.csv)
    slab_dimensions=${slab_dimensions##*: }
    x_dimension=${slab_dimensions%%,*}
    x_dimension=$( awk "BEGIN{ print $x_dimension * 0.5; exit}" )
    x_dimension=$(printf "%.3f" $x_dimension)
    y_dimension=${slab_dimensions%,*}
    y_dimension=${y_dimension#*,}
    y_dimension=$( awk "BEGIN{ print $y_dimension * 0.5; exit}" )
    y_dimension=$(printf "%.3f" $y_dimension)
    z_dimension=${slab_dimensions##*,}
    z_dimension=$( awk "BEGIN{ print $z_dimension * 0.5; exit}" )
    z_dimension=$(printf "%.3f" $z_dimension)
    echo "  INDUS slab dimensions: x: -$x_dimension,$x_dimension  y: -$y_dimension,$y_dimension  z: -$z_dimension,$z_dimension"

    x_min=$( awk "BEGIN{ print $x_center - $x_dimension; exit}" )
    x_max=$( awk "BEGIN{ print $x_center + $x_dimension; exit}" )
    y_min=$( awk "BEGIN{ print $y_center - $y_dimension; exit}" )
    y_max=$( awk "BEGIN{ print $y_center + $y_dimension; exit}" )
    z_min=$( awk "BEGIN{ print $z_center - $z_dimension; exit}" )
    z_max=$( awk "BEGIN{ print $z_center + $z_dimension; exit}" )
    echo "  INDUS slab ranges: x: $x_min $x_max  y: $y_min $y_max  z: $z_min $z_max"

    start_num_weak=$(tail -n 1 $wd/$output_folder/${sim_name}_num_waters.csv)
    start_num_weak=${start_num_weak##*: }
    echo "  number of waters in INDUS slab: $start_num_weak"

    num_windows_weak=$( awk "BEGIN{ print ($start_num_weak - $START_NUM_STRONG - $NUM_INCR_STRONG) / $NUM_INCR_WEAK + 1; exit}" )
    num_windows_strong=$( awk "BEGIN{ print $START_NUM_STRONG / $NUM_INCR_STRONG + 1; exit}" )
fi

if [ $RUN_INIT -eq 1 ]; then
    mkdir -p init
    # copy in needed files
    cp -rv $wd/$forcefield init/
    cp -v $wd/equil/${sim_name}_equil.gro init/
    cp -v $wd/$input_folder/${sim_name}.top init/

    # launch
    cd init

    # Initially set N waters inside cavity
    echo "Relaxing cavity to initial N"
    cp -v $wd/$input_folder/nvt_cavity_relax.mdp ./nvt_cavity_relax.mdp
    sed -i s/SIMTEMP/$sim_temperature/g ./nvt_cavity_relax.mdp

    cp -v $wd/$input_folder/indus.input ./indus.input
    sed -i s/GRONAME/"${sim_name}_equil.gro"/g indus.input
    sed -i s/XRANGE/"$x_min $x_max"/g indus.input
    sed -i s/YRANGE/"$y_min $y_max"/g indus.input
    sed -i s/ZRANGE/"$z_min $z_max"/g indus.input

    cp -v $wd/$input_folder/plumed_indus_template.dat ./plumed_indus_relax.dat
    sed -i s/INDUSFILE/"indus.input"/g ./plumed_indus_relax.dat
    sed -i s/BIAS/$SPRING_STRONG/g ./plumed_indus_relax.dat
    sed -i s/NUMWATER/$start_num_weak/g plumed_indus_relax.dat
    sed -i s/MAXWATER/$start_num_weak/g plumed_indus_relax.dat
    sed -i s/DUMPSTEP/'99980'/g plumed_indus_relax.dat

    # first adjust box to start num using normal restraint
    mpiexec -np 1 gmx_mpi grompp -f nvt_cavity_relax.mdp -o ${sim_name}_relax -c ${sim_name}_equil.gro -p ${sim_name}.top
    mpiexec -np $num_mpi gmx_mpi mdrun -ntomp $num_omp -deffnm ${sim_name}_relax -plumed plumed_indus_relax.dat

    echo "Generating initial INDUS configurations from steered MD"
    # calculating pulling windows and pulling time
    pull_windows=$(echo "$start_num_weak / $NUM_INCR_STRONG + 1" | bc)
    # Total number of steps to pull, rounded to nearest integer
    pull_steps=$(echo "(($pull_rate * $NUM_INCR_STRONG * $pull_windows)+0.5)/1" | bc)
    # Total amount of time to pull, rounded to nearest integer
    pull_time=$(echo "$pull_steps / 500" | bc)

    cp -v $wd/$input_folder/nvt_steered.mdp ./nvt_steered.mdp
    # edit ndp inputs
    sed -i s/SIMTEMP/$sim_temperature/g ./nvt_steered.mdp
    sed -i s/PULLTIME/$pull_time/g nvt_steered.mdp
    sed -i s/PULLSTEPS/$pull_steps/g nvt_steered.mdp

    cp -v $wd/$input_folder/plumed_indus_steered.dat ./
    sed -i s/INDUSFILE/"indus.input"/g plumed_indus_steered.dat
    sed -i s/STARTNUM/$start_num_weak/g plumed_indus_steered.dat
    sed -i s/PULLSTEPS/$pull_steps/g plumed_indus_steered.dat
    sed -i s/BIAS/$bias_const/g plumed_indus_steered.dat 

    # now decrease box water as a function of time (steered MD)
    mpiexec -np 1 gmx_mpi grompp -f nvt_steered.mdp -o ${sim_name}_steered -c ${sim_name}_relax.gro -p ${sim_name}.top
    mpiexec -np $num_mpi gmx_mpi mdrun -ntomp $num_omp -deffnm ${sim_name}_steered -plumed plumed_indus_steered.dat

    # remove unnecessary files
    rm -f \#*
    rm -f bck.*
    rm -f analysis.*
    cd ../

    echo "Extracting initial INDUS configurations"
    mkdir -p extract_frames
    cd extract_frames/   

    # How often to extract frame from trajectory (for extract frames) - in ps, assuming 0.002 fs timestep
    pull_incr=$(echo "$pull_rate * $NUM_INCR_STRONG / 500" | bc)

    # iterate
    for (( i=0; i<$pull_windows; i++)); do

        time=$(($i * $pull_incr))
        water=$(echo "$start_num_weak - $i * $NUM_INCR_STRONG" | bc)

        mpiexec -np 1 gmx_mpi trjconv -f ../init/${sim_name}_steered.xtc -s ../init/${sim_name}_steered.tpr -dump ${time} -o ${sim_name}_${water}.gro -pbc whole<< INPUT
SYSTEM
INPUT
    done
    cd ../
fi

if [ $RUN_UMBRELLA -eq 1 ]; then
    echo ""
    echo "Running INDUS simulations"
    mkdir -p umbrella
    cd umbrella/

    # # iterate over each frame and run the corresponding job
    n_windows=0
    array=()
    for (( i=0; i<$num_windows_weak; i++)); do
        water=$(echo "$start_num_weak - $i * $NUM_INCR_WEAK" | bc)
        array+=("$water")
        mkdir -p indus_${water}
        cd indus_${water}

        # copy umbrella sampling file
        cp -rv $wd/$forcefield ./
        cp -v $wd/$input_folder/${sim_name}.top ./
        cp -v $wd/extract_frames/${sim_name}_${water}.gro ./
        cp -v $wd/run_indus_window.sh ./run_indus_window${water}.sh

        sim_steps=$(awk "BEGIN{ print $sim_time * 1000 / 0.002; exit}") # number of simulation steps (sim_time (ps)/0.002 (ps))
        dump_step=$(( $sim_steps - 20 ))
        cp -v $wd/$input_folder/us_prod.mdp ./us_prod.mdp
        sed -i s/SIMTEMP/$sim_temperature/g ./us_prod.mdp
        sed -i s/NUMSTEPS/$sim_steps/g ./us_prod.mdp
        sed -i s/SIMTIME/$sim_time/g ./us_prod.mdp

        cp -v $wd/$input_folder/indus.input ./indus.input
        sed -i s/GRONAME/"${sim_name}_${water}.gro"/g indus.input
        sed -i s/XRANGE/"$x_min $x_max"/g indus.input
        sed -i s/YRANGE/"$y_min $y_max"/g indus.input
        sed -i s/ZRANGE/"$z_min $z_max"/g indus.input

        cp -v $wd/$input_folder/plumed_indus_template.dat ./plumed_indus_${water}.dat
        sed -i s/INDUSFILE/"indus.input"/g ./plumed_indus_${water}.dat
        sed -i s/BIAS/$SPRING_WEAK/g ./plumed_indus_${water}.dat
        sed -i s/NUMWATER/$water/g ./plumed_indus_${water}.dat
        sed -i s/MAXWATER/$start_num_weak/g ./plumed_indus_${water}.dat
        sed -i s/DUMPSTEP/$dump_step/g ./plumed_indus_${water}.dat

        # run grompp
        mpiexec -np 1 gmx_mpi grompp -f us_prod.mdp -o ${sim_name}_indus_${water} -c ${sim_name}_${water}.gro -p ${sim_name}.top
        n_windows=$(($n_windows+1))
        cd ../
    done
    
    # # iterate over each frame and run the corresponding job
    for (( i=0; i<$num_windows_strong; i++)); do
        water=$(echo "$START_NUM_STRONG - $i * $NUM_INCR_STRONG" | bc)
        array+=("$water")
        mkdir -p indus_${water}
        cd indus_${water}

        # copy umbrella sampling file
        cp -rv $wd/$forcefield ./
        cp -v $wd/$input_folder/${sim_name}.top ./
        cp -v $wd/extract_frames/${sim_name}_${water}.gro ./
        cp -v $wd/run_indus_window.sh ./run_indus_window${water}.sh

        sim_steps=$(awk "BEGIN{ print $sim_time * 1000 / 0.002; exit}") # number of simulation steps (sim_time (ps)/0.002 (ps))
        dump_step=$(( $sim_steps - 20 ))
        cp -v $wd/$input_folder/us_prod.mdp ./us_prod.mdp
        sed -i s/SIMTEMP/$sim_temperature/g ./us_prod.mdp
        sed -i s/NUMSTEPS/$sim_steps/g ./us_prod.mdp
        sed -i s/SIMTIME/$sim_time/g ./us_prod.mdp

        cp -v $wd/$input_folder/indus.input ./indus.input
        sed -i s/GRONAME/"${sim_name}_${water}.gro"/g indus.input
        sed -i s/XRANGE/"$x_min $x_max"/g indus.input
        sed -i s/YRANGE/"$y_min $y_max"/g indus.input
        sed -i s/ZRANGE/"$z_min $z_max"/g indus.input

        cp -v $wd/$input_folder/plumed_indus_template.dat ./plumed_indus_${water}.dat
        sed -i s/INDUSFILE/"indus.input"/g ./plumed_indus_${water}.dat
        sed -i s/BIAS/$SPRING_STRONG/g ./plumed_indus_${water}.dat
        sed -i s/NUMWATER/$water/g ./plumed_indus_${water}.dat
        sed -i s/MAXWATER/$start_num_weak/g ./plumed_indus_${water}.dat
        sed -i s/DUMPSTEP/$dump_step/g ./plumed_indus_${water}.dat

        # run grompp
        mpiexec -np 1 gmx_mpi grompp -f us_prod.mdp -o ${sim_name}_indus_${water} -c ${sim_name}_${water}.gro -p ${sim_name}.top
        n_windows=$(($n_windows+1))
        cd ../
    done

    n_cores=$( nproc --all )
    cores_per_window=$( awk "BEGIN{ print ( $n_nodes * $SLURM_NNODES ) / $n_windows; exit}")

    n=0
    node=0
    for water in "${array[@]}"; do
        cd indus_${water}
        export OMP_NUM_THREADS=$cores_per_window
        true_offset=$( awk "BEGIN{ print $n * $n_mpi * $cores_per_window; exit }")
        if [ "$(( ($node+1) * $n_cores ))" -le "$true_offset" ]; then
            node=$(($node+1))
            offset=0
        else
            offset=$( awk "BEGIN{ print $true_offset - $node * $n_cores; exit }")
        fi

        gmxcmd="$( gmx_mpi mdrun -v -ntomp $cores_per_window -deffnm ${sim_name}_indus_${water} -plumed plumed_indus_${water}.dat -pin on -pinoffset $offset -pinstride 1 )"
        srun -l -N 1 -n 1 -r "$node" $gmxcmd > slurm_${water}.out &
        n=$(($n+1))
        cd ../
    done
    ## WAIT FOR ALL PROCESSES TO FINISH
    wait
    cd ../
fi

if [ $RUN_WHAM -eq 1 ]; then
    echo ""
    echo "Running WHAM"

    mkdir -p wham

    # first, edit umbrella sampling trajectories to account for equilibration time
    cd umbrella

    # First, truncate each file (discrete/smeared files), then combine into a single file
    # second, combine histogram files
    # third, populate metadata file - treat x coord as the discrete num (no spring const), y coord as smeared num
    > $wd/wham/${sim_name}_metadata.dat # create empty metadata file
    for (( i=0; i<$num_windows_weak; i++)); do
        water=$(echo "$start_num_weak - $i * $NUM_INCR_WEAK" | bc)
        cd indus_${water}
        # get number of waters from name
        echo "combining indus files for indus_${water}"
        # combine into a single file
        python $py_truncate -w $PWD/ -f water_num.${water}.csv -o truncated_water_combined.${water}.csv -b $START_TIME -e $END_TIME

        # combining histograms visualizing overlap as in Gromacs
        if [ $i -eq 0 ]; then
            python $py_histogram -w $PWD/ -d $wd/wham/ -t histo_${water}.csv -o combined_histogram.csv -n 1
        else
            python $py_histogram -w $PWD/ -d $wd/wham/ -t histo_${water}.csv -o combined_histogram.csv -n 0
        fi

        # populate metadata file
        echo "$PWD/truncated_water_combined.${water}.csv ${water} ${water} 0.0 $spring_weak_kcal " >> $wd/wham/${sim_name}_metadata.dat

        cd ../
    done

    # populate metadata file
    for (( i=0; i<$num_windows_strong; i++)); do
        water=$(echo "$START_NUM_STRONG - $i * $NUM_INCR_STRONG" | bc)
        cd indus_${water}
        # get number of waters from name
        echo "combining indus files for indus_${water}"
        # combine into a single file
        python $py_truncate -w $PWD/ -f water_num.${water}.csv -o truncated_water_combined.${water}.csv -b $START_TIME -e $END_TIME
        
        # combining histograms visualizing overlap as in Gromacs
        python $py_histogram -w $PWD/ -d $wd/wham/ -t histo_${water}.csv -o combined_histogram.csv -n 0
        
        # populate metadata file
        echo "$PWD/truncated_water_combined.${water}.csv ${water} ${water} 0.0 $spring_strong_kcal " >> $wd/wham/${sim_name}_metadata.dat

        cd ../
    done
    cd ../

    # As set, each bin corresponds to 1 water molecules. 
    bin_min=-0.5
    bin_max=$( awk "BEGIN{ print $start_num_weak + 0.5; exit}" )
    num_bins=$( awk "BEGIN{ print $start_num_weak + 1; exit}" )

    # run wham (note: combining histograms is not necessary, only for visualizing overlap as in Gromacs)
    cd wham
    $wham_analyis Px=0 $bin_min $bin_max $num_bins Py=0 $bin_min $bin_max $num_bins $WHAM_TOLERANCE $sim_temperature 0 $PWD/${sim_name}_metadata.dat $PWD/${sim_name}_wham.csv 0 > $PWD/wham.log

    python $py_analysis -w $PWD/ -p $wd/$output_folder/ -f ${sim_name}_wham.csv -o ${sim_name}_wham_reweighted.csv -T $sim_temperature

    # final output is in ${sim_name}_wham_reweighted.csv
    cd $wd
    echo "*** WHAM analysis complete ***"
fi
