#!/bin/bash

# Source this file before each SAM prep/run

# prepare/run hydrophobicity simulation path variables
PATH_HOME=$(echo $HOME)                                     # path to home directory (this one is true for swarm/aci/comet, stampede is different)
PATH_PREP_SCRIPTS=$PATH_HOME/indus                          # path to system preparation scripts
PATH_MDLIGANDS=$HOME/MDLigands                              # path to MDLigand repository
PATH_FF=$PATH_MDLIGANDS/forcefield                          # path to force fields
PATH_LIGANDS=$PATH_MDLIGANDS/final_ligands                  # path to ligands
PATH_RUN_SCRIPTS=$PATH_PREP_SCRIPTS/run_files               # path to simulation run scripts
PATH_ANALYSIS_SCRIPTS=$PATH_HOME/analysis_files             # path to analysis scripts
PATH_SIMULATIONS=$PATH_HOME/simulations                     # path to simulation files
PATH_PATTERNS=$PATH_PREP_SCRIPTS/pattern_files              # path to patterns
PATH_SOLVENT=$PATH_PREP_SCRIPTS/solvents                    # path to solvents
PATH_MDBUILDER=$PATH_HOME/bin/python_modules/MDBuilder      # path to MDBuilder
