#!/bin/bash

## CHECK THAT ALL INPUTS TO PREPARE INDUS ARE VALID

# --- Load simulation details ---
source $HOME/hydrophobicity_project/hydrophobicity_rc.sh
source ./indus_prep_details.sh

# --- Define repeated files and paths ---
# set path variables
py_prep_file=$PATH_MDBUILDER/builder/make_planar_sam.py
py_add_position_restraint=$PATH_MDBUILDER/core/add_position_restraint.py

# check simulation details
if [ $PREPARE_SYSTEM -ne 1 ]; then
    exit
fi

ensemble=$( echo $ENSEMBLE | tr [:upper:] [:lower:] )
if [ $ensemble != "nvt" ]; then
    echo "  INDUS simulations must be performed using the NVT ensemble at this time"; exit
fi

if [ ! -e $PATH_FF/$FORCEFIELD ]; then
    echo "  ERROR! Force field not selected or unavailable"; exit
fi

if [ -z $SIM_TEMPERATURE ]; then
    echo "  ERROR! Temperature not specified"; exit
fi

if [ ! -e $PATH_FF/$FORCEFIELD/${WATER_MODEL}.itp ]; then
    echo "  ERROR! Water model not selected or unavailable"; exit
fi

if [[ -z $COSOLVENT || $COSOLVENT != 'none' && $COSOLVENT != 'methanol' ]]; then
    echo "  ERROR! Cosolvent not specified or unavailable"; exit
fi

if [[ -z $COSOLVENT_FRACTION || $(awk "BEGIN{ if ($COSOLVENT_FRACTION > 1.0) print 1; else print 0}") -eq 1 || $(awk "BEGIN{ if ($COSOLVENT_FRACTION < 0.0) print 1; else print 0}") -eq 1 ]]; then
    echo "  ERROR! Cosolvent fraction not specified or not > 0 and < 1"; exit
fi

if [[ -z $SOLVENT_HEIGHT || $(awk "BEGIN{ if ($SOLVENT_HEIGHT < 0.0) print 1; else print 0}") -eq 1 ]]; then
    echo "  ERROR! Solvent height not specified or negative"; exit
fi

sam_type=$( echo $SAM_TYPE | tr [:upper:] [:lower:] )
# check system specific details
if [ $sam_type != 'none' ]; then
    # separate ligand string (if two ligands are specified)
    ligand1=$( echo "$SAM_LIGANDS" | cut -f1 -d, )
    ligand2=$( echo "$SAM_LIGANDS" | cut -f2 -d, )
    # check that ligands are available in $PATH_LIGANDS/$FORCEFIELD/
    match=0
    for check_ligand in $PATH_LIGANDS/$FORCEFIELD/*; do
        check_ligand=${check_ligand##*$path_ligands\/}
        if [[ $ligand1 == $check_ligand ]]; then
            match=$(($match+1))
        fi
        
        if [[ $ligand2 == $check_ligand ]]; then
            match=$(($match+1))
        fi
    done

    if [ $match -lt 2 ]; then
        echo "  ERROR! $ligand1 or $ligand2 unavailable. Check input."; exit
    fi
    
    # check that ligand fration sums to 1
    fract1=$( echo "$LIGAND_FRACTION" | cut -f1 -d, )
    fract2=$( echo "$LIGAND_FRACTION" | cut -f2 -d, )
    if [ $(awk "BEGIN{ if ($fract1 < 1.0 && $fract1 > 0.0) print 1; else print 0}") -eq 1 ]; then
        if [ $(awk "BEGIN{ if ($fract1 + $fract2 != 1.0) print 1; else print 0}") -eq 1 ]; then
            echo "  ERROR! Ligand fractions do not sum to 1."; exit
        fi
    elif [ $(awk "BEGIN{ if ($fract1 > 1.0 || $fract1 < 0.0) print 1; else print 0}") -eq 1 ]; then
        echo "  ERROR! Ligand fraction must be > 0 and <= 1."; exit
    fi

    if [[ -z $NUM_LIG_X || -z $NUM_LIG_Y ]]; then
        echo "  ERROR! Please specify dimension of SAM"; exit
    fi
    
    if [[ -z $SOLVENT_HEIGHT || $( awk "BEGIN{ if ($SOLVENT_HEIGHT < 0.0) print 1; else print 0}") -eq 1 ]]; then
        echo "  ERROR! Please specify valid height of solvent"; exit
    fi

    if [[ -z $LAYER_SEPARATION || $( awk "BEGIN{ if ($LAYER_SEPARATION < 0.0) print 1; else print 0}") -eq 1 ]]; then
        echo "  ERROR! Please specify valid distance between SAMs"; exit
    fi
fi

# check pattern on SAM
if [ -z $PATTERN ]; then
    PATTERN='random'
fi

pattern=$( echo $PATTERN | tr [:upper:] [:lower] ) #  || $pattern != 'checker' || $pattern != 'janus' ]
if [[ $pattern != 'random' && $pattern != 'checker' && $pattern != 'wide_checker' && $pattern != 'janus' && $pattern != 'single' ]]; then
    echo "  ERROR! Only random, checker, and janus are available. Please specify a valid SAM pattern"; exit
fi

echo "***  Simulation details specified correctly ***"
echo ""
