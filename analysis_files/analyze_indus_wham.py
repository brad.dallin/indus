# -*- coding: utf-8 -*-
##############################################################################
#
# Author: Bradley C. Dallin
# email: bdallin@wisc.edu
# 
##############################################################################

"""
analyze_indus_wham.py

Renormalizes and integrates out unnecessary probabilities here. 

Assumes first column (x-values) correspond to probability distribution of interest, so 
integrates out values in second column (y- values). Also assumes layout of 
input file is frm Alan Grossfield WHAM, where all y-values for a given x-value
are listed consecutively. 

Algorithm:
     - From output FE, calculate (unnormalized) probability for each line
     - Sum probabilities for each x-value over all y-values
     - Renormalize probabilities, output P(x)

@author: 
    Reid van Lehn
    Bradley C. Dallin (modified for python2)
    
** UPDATES **

"""

##############################################################################
# Imports
##############################################################################

from __future__ import print_function, division
from optparse import OptionParser # Used to allow commands within command line
import numpy as np 

##############################################################################
# Classes
##############################################################################
class analyze_wham:
    '''
    Class to analyze wham data from INDUS
    '''
    def __init__( self, wd, out_path, in_file, out_file, T ):
        # collect inputs
        self.wd = wd  # working directory
        self.out_path = out_path            # output directory
        self.in_file = in_file              # wham file input
        self.out_file = out_file            # reweighted output filename
        self.T = T                          # simulation temperature
        self.kB = 1.9871708e-3              # Boltzmann constant in kcal
        self.kT = self.kB * self.T          # kT in kcal/mol
        self.neg_beta = -1.0/self.kT        # - 1 / beta
        self.kcal2kJ = 4.184                # 4.184 kJ / 1 kcal
        
    def read_wham( self ):
        '''
        Function to read text from input wham file
        
        INPUT
        -----
        self
        
        OUTPUT
        ------
        newData : np.array, shape[ N, N, 2 ], dtype = float
            An array containing the unnormalized probabilities and free energies at each x and y 
            position. (x and y are each N, it is how it is output from 2d wham)
            np.array[ :, :, 0 ] = probabilities
            np.array[ :, :, 1 ] = free energies
        '''
        fullPath2File = self.wd + self.in_file
        print( '  Loading data from: {:s}'.format( fullPath2File ) )
        
        with open( fullPath2File, 'r' ) as outputfile: # Opens gro file and make it writable
            fileData = outputfile.readlines()
    
        data = [ line for line in fileData if '#' not in line ]
        data = [ n.strip( '\n' ) for n in data ]
        data = [ n.split( ) for n in data if len(n) > 0 ]
        data = [ [ float( n ) for n in line if n != '' ] for line in data ]
        
        # Turn into a 2D array
        N = np.sqrt( len(data) ).astype('int')
        newData = np.zeros(( N, N, 2 ))
        for line in data:
            newData[int(line[0]),int(line[1]),0] = line[2]
            newData[int(line[0]),int(line[1]),1] = line[3]
        
        print( '*** Data loaded ***' )
        
        return newData
        
    def sum_prob( self ):
        '''
        Function to normalize the wham data
        
        INPUT
        -----
        self
        
        OUTPUT
        ------
        normData : np.array, shape[ N, 4 ], dtype = float
            An array containing the the normalized probabilities, free energy,
            and normalized free energy (to the min FE) as a function of the reaction
            coordinate np.array( reaction coord, prob, fe, norm_fe )
        '''
        data = self.read_wham()
        unnormProb = np.exp( data[:,:,0] * self.neg_beta )
        unnormProbSum = unnormProb.sum()
        print( '  Unnormalized sum: {:0.3f}'.format( unnormProbSum ) )
        
        coord = np.arange( 0, data.shape[0] ).astype('float')
        probSum = unnormProb.sum( axis = 1 ) / unnormProbSum
        print( '  Normalized sum: {:0.3f}'.format( probSum.sum() ) )
        
        fe = -1.0 * np.log( probSum )
#        fe = -1.0 * self.kT * np.log( probSum )
#        fe = -1.0 * self.kT * np.log( probSum ) * self.kcal2kJ
        print( '  Min FE: {:0.3f} kT'.format( fe.min() ) )
#        print( '  Min FE: {:0.3f} kcal/mol'.format( fe.min() ) )
#        print( '  Min FE: {:0.3f} kJ/mol'.format( fe.min() ) )
        
        normData = np.stack(( coord, probSum, fe, fe - fe.min() )).transpose()
        
        return normData
    
    def write_fe( self ):
        '''
        writes out final data to out_path/out_file
        '''
        data = self.sum_prob()
        outfile = open( self.out_path + self.out_file, 'w+' )
        outfile.write( '# Free energy data from INDUS\n\n' )
        outfile.write( '# {:>1s} {:>10s} {:>12s} {:>12s}\n'.format( 'N', 'p(N)', 'mu (kT)', 'FE (kT)' ) )
#        outfile.write( '# {:>1s} {:>10s} {:>12s} {:>12s}\n'.format( 'N', 'p(N)', 'mu (kcal/mol)', 'FE (kcal/mol)' ) )
#        outfile.write( '# {:>1s} {:>10s} {:>12s} {:>12s}\n'.format( 'N', 'p(N)', 'mu (kJ/mol)', 'FE (kJ/mol)' ) )
        for line in data:
            outfile.write( '{:3d}, {:9.3e}, {:11.3f}, {:11.3f}\n'.format( int(line[0]), line[1], line[2], line[3] ) )
        
        outfile.close()
        
        return 0      
                
#%%
##############################################################################
# Execute script
##############################################################################

if __name__ == "__main__":    
    # --- Define path variables ---
    print( '*** analyzing mu excess simulation ***' )
    
    testing = 'False'
    if ( testing == 'True' ):  
        in_name = 'sam_wham.csv'
        out_name = 'sam_wham_reweighted.csv'
        in_path = r'R:/simulations/polar_sams/robustness_checks/sam_single_12x12_300K_C12CONH2_tip3p_nvt_CHARMM36_2x2x0.3nm/wham/'
        out_path = r'R:/simulations/polar_sams/robustness_checks/sam_single_12x12_300K_C12CONH2_tip3p_nvt_CHARMM36_2x2x0.3nm/output_files/'
        T = 300     
        
    else:  
        # Adding options for command line input (e.g. --maxz, etc.)
        use = 'Usage: %prog [options]'
        parser = OptionParser(usage = use)
        parser.add_option( '-f', '--in', dest = 'in_name', action = 'store', type = 'string', help = 'input file', default = '.' )
        parser.add_option( '-o', '--out', dest = 'out_name', action = 'store', type = 'string', help = 'output file', default = '.' )
        parser.add_option( '-w', '--wd', dest = 'in_path', action = 'store', type = 'string', help = 'working directory', default = '.' )
        parser.add_option( '-p', '--op', dest = 'out_path', action = 'store', type = 'string', help = 'output directory', default = '.' )
        parser.add_option( '-T', '--temp', dest = 'T', action = 'store', type = 'float', help = 'simulation temperature', default = '300' )
        
        
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        # assign input parameters
        in_name = options.in_name
        out_name = options.out_name
        in_path = options.in_path
        out_path = options.out_path
        T = options.T
    
    #%%
    analyze_wham = analyze_wham( in_path, out_path, in_name, out_name, T )
    analyze_wham.write_fe()
#    time, data = analyze_wham.check_convergence()
    