#!/bin/bash

sim_name=SIMNAME
BEGIN_TIME=0
END_TIME=2000
CONVERGE_TIME=1000
STEP=100

# # set path variables
# source $HOME/indus/indus_rc.sh
# source ./indus_details.sh
# wd="$PWD"

## Ensure only wham is run
sed -i s/'RUN_EQUIL=1'/'RUN_EQUIL=0'/g ./indus_details.sh
sed -i s/'CALC_DIMS=1'/'CALC_DIMS=0'/g ./indus_details.sh
sed -i s/'RUN_INIT=1'/'RUN_INIT=0'/g ./indus_details.sh
sed -i s/'RUN_UMBRELLA=1'/'RUN_UMBRELLA=0'/g ./indus_details.sh
sed -i s/'RUN_WHAM=0'/'RUN_WHAM=1'/g ./indus_details.sh

## Change start and end times
init_time="$( grep "START_TIME" indus_details.sh | awk '{ print $1 }' )"
final_time="$( grep "END_TIME" indus_details.sh | awk '{ print $1 }' )"

echo "*** CHECKING INDUS EQUILIBRATION ***"
> output_files/indus_equilibration.csv
echo "# indus equilibration" >> output_files/indus_equilibration.csv
echo "" >> output_files/indus_equilibration.csv
echo " t (ps) mu (kT)" >> output_files/indus_equilibration.csv
n_steps=$( awk "BEGIN{ print ( $END_TIME - $CONVERGE_TIME ) / $STEP; exit}")

for (( ii=0; ii<=$n_steps; ii++ )); do
    echo "Calculating $ii of $n_steps"
    frame=$( awk "BEGIN{ print $ii * $STEP; exit}")
    end_frame=$( awk "BEGIN{ print $frame + $CONVERGE_TIME; exit}")
    sed -i s/"$init_time"/"START_TIME=${frame}"/g indus_details.sh
    sed -i s/"$final_time"/"END_TIME=${end_frame}"/g indus_details.sh
    bash run_indus.sh &>/dev/null
    mu=$( head -n 4 output_files/${sim_name}_wham_reweighted.csv | tail -n 1 | awk '{ print $NF }' | bc )
    echo "${frame},${mu}" >> output_files/indus_equilibration.csv
    init_time="START_TIME=${frame}"
    final_time="END_TIME=${end_frame}"
done

# echo "*** CHECKING INDUS CONVERGENCE ***"
# > output_files/indus_convergence.csv
# echo "# indus convergence" >> output_files/indus_convergence.csv
# echo "" >> output_files/indus_convergence.csv
# echo " t (ps) mu (kT)" >> output_files/indus_convergence.csv
# sed -i s/"$init_time"/"START_TIME=${BEGIN_TIME}"/g indus_details.sh
# n_steps=$( awk "BEGIN{ print ( $END_TIME - $BEGIN_TIME ) / $STEP; exit}")

# for (( ii=1; ii<=$n_steps; ii++ )); do
#     echo "Calculating $ii of $n_steps"
#     frame=$( awk "BEGIN{ print $BEGIN_TIME + $ii * $STEP; exit}")
#     sed -i s/"$final_time"/"END_TIME=${frame}"/g indus_details.sh
#     bash run_indus.sh &>/dev/null
#     mu=$( head -n 4 output_files/sam_wham_reweighted.csv | tail -n 1 | awk '{ print $NF }' | bc )
#     echo "${frame},${mu}" >> output_files/indus_convergence.csv
#     final_time="END_TIME=${frame}"
# done