##############################################################################
# hydrophobicity: A Python Library for analysis of an water-soft material 
#                 interface
#
# Author: Bradley C. Dallin
# email: bdallin@wisc.edu
# 
##############################################################################

##############################################################################
# Imports
##############################################################################

#import sys
import multiprocessing
import numpy as np

__all__ = [ 'parallel' ]

##############################################################################
# Monolayer info class
##############################################################################
def worker( arg ):
    R''' '''
    obj, traj = arg
    return obj.compute( traj )

class parallel:
    R''' '''
    def __init__( self, traj, func, args, n_procs = 20, average = True ):
        R''' '''
        ### PRINTING
        print("**** CLASS: %s ****"%(self.__class__.__name__))
        
        # Number of processors available
        if n_procs < 0:
            n_procs = multiprocessing.cpu_count()
        print( "\n--- Running %s on %s cores ---\n" %( func.__name__, str(n_procs) ) )
                
        traj_list = self.split_traj( traj, n_procs )
        # delete original trajectory to reduce memory use
        del traj 
        
        object_list = self.create_obj_list( func, n_procs, traj_list, args )
    
        pool = multiprocessing.Pool( processes = n_procs )

        result_list = pool.map( worker, ( ( obj, trj ) for obj, trj in zip( object_list, traj_list ) ) )
        pool.close()
        pool.join()

#        self.results = result_list # for convergence        
        if average:
            self.results = result_list[0] # np.array(result_list).mean(axis=0)
            for result in result_list[1:]:
                self.results += result
            
            self.results /= len(result_list)
        else:
            self.results = result_list[0] # np.array(result_list).mean(axis=0)
            for result in result_list[1:]:
                self.results += result
    
    @staticmethod
    def split_traj( traj, n_procs ):
        R''' '''
        traj_list = []
        if n_procs > 1:
            ## SPLIT TRAJ INTO N PROCESSES
            len_split = len(traj) // n_procs
            remainder = len(traj) % n_procs
            splits = []
            for n in range( n_procs ):
                if n < remainder:
                    splits.append( len_split + 1 )
                else:
                    splits.append( len_split )
            
            current = 0
            for split in splits:
                traj_list.append( traj[current:current+split] )
                current += split
        else:
            traj_list.append( traj )
            
        return traj_list
    
    @staticmethod
    def create_obj_list( func, n_procs, traj_list, args ):
        R''' '''
        ## CREATES A LIST OF CLASS OBJECTS
        object_list = [ func( **args ) for ii in range( 0, n_procs ) ]
        
        if len(object_list) != len(traj_list):
            raise RuntimeError( '\n  ERROR! More objects created than split trajectories' )
        
        return object_list
  