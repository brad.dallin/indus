# -*- coding: utf-8 -*-
##############################################################################
#
# Author: Bradley C. Dallin
# email: bdallin@wisc.edu
# 
##############################################################################


from __future__ import print_function, division
from optparse import OptionParser # Used to allow commands within command line
import numpy as np

##############################################################################
# Classes and Functions
##############################################################################
class indus_data:
    '''
    Class containing discrete and smeared INDUS data.
    '''
    def __init__( self, wd, infile, outfile, start = 1000, end = -1 ):
        # collect inputs
        self.wd = wd
        self.infile = infile
        self.outfile = outfile
        self.start = start
        self.end = end

    def load_csv( self, filename ):
        '''
        Loads a .csv file and returns the data as an np.array
        '''
        fullPath2File = self.wd + filename
        with open( fullPath2File, 'r' ) as outputfile: # Opens gro file and make it writable
            fileData = outputfile.readlines()
    
        data = [ line for line in fileData if '#' not in line ]
        data = [ n.strip( '\n' ) for n in data ]
        data = [ n.split( ) for n in data ]
        data = np.array( [ [ float( n ) for n in line if n != '' ] for line in data ] )

        if self.end > 0:
            ndx = int( self.end / ( data[1,0] - data[0,0] ) )
            data = data[:ndx,:]
        
        if self.start > 0:
            ndx = int( self.start / ( data[1,0] - data[0,0] ) )
            data = data[ndx:,:]
        
        return data
    
    def write_file( self ):
        '''
        Write out new truncated .csv file
        '''
        data = self.load_csv( self.infile )
        
        # write out mu data profile
        fullPath2File = self.wd + self.outfile
        print( '  Outputting file to {:s}'.format( fullPath2File ) )
        outfile = open( fullPath2File, 'w+' )
        for line in data:
            outfile.write( '{:0.6f} {:0.6f} {:0.6f}\n'.format( line[0], line[1], line[2] ) )
            
        outfile.close()
        
        return 0
                
##############################################################################
# Execute script
##############################################################################

if __name__ == "__main__":    
    # --- Define path variables ---
    print( '*** Combining INDUS data ***' )
    
    testing = 'False'
    if ( testing == 'True' ):
        wd = r'R:/simulations/test_indus/n11_plumed_indus_1ps/indus_z38.95/umbrella/indus_40.0/'
        inname = 'plumed.out'
        outname = 'truncate_water_combined.45.0.csv'
        start = 2000
        end = 4000
        
    else:  
        # Adding options for command line input (e.g. --maxz, etc.)
        use = 'Usage: %prog [options]'
        parser = OptionParser(usage = use)
        parser.add_option( '-w', '--wd', dest = 'wd', action = 'store', type = 'string', help = 'working directory', default = '.' )
        parser.add_option( '-f', '--in', dest = 'inname', action = 'store', type = 'string', help = 'input file', default = '.' )
        parser.add_option( '-o', '--out', dest = 'outname', action = 'store', type = 'string', help = 'output file', default = '.' )
        parser.add_option( '-b', '--begin', dest = 'start', action = 'store', type = 'float', help = 'Equilibration time', default = '1000' )
        parser.add_option( '-e', '--end', dest = 'end', action = 'store', type = 'float', help = 'Last frame considered', default = '-1' )
        
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        # assign input parameters
        wd = options.wd
        inname = options.inname
        outname = options.outname
        start = options.start
        end = options.end
    
    indus_data = indus_data( wd, inname, outname, start = start, end = end )
    data = indus_data.load_csv( inname )
    indus_data.write_file()