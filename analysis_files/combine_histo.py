# -*- coding: utf-8 -*-
##############################################################################
#
# Author: Bradley C. Dallin
# email: bdallin@wisc.edu
# 
##############################################################################


from __future__ import print_function, division
from optparse import OptionParser # Used to allow commands within command line
import numpy as np

##############################################################################
# Classes and Functions
##############################################################################
class indus_histo_data:
    '''
    Class containing discrete and smeared INDUS data.
    '''
    def __init__( self, mainDirectory, outDir, histoFilename, outFilename, newData = True ):
        # collect inputs
        self.mainDirectory = mainDirectory
        self.outDir = outDir
        self.histoFilename = histoFilename
        self.outFilename = outFilename
        self.newData = newData

    def load_csv( self, fullPath2File ):
        '''
        Loads a .csv file and returns the data as an np.array
        '''
        with open( fullPath2File, 'r' ) as outputfile: # Opens gro file and make it writable
            fileData = outputfile.readlines()
    
        data = [ line for line in fileData if '#' not in line ]
        data = [ n.strip( '\n' ) for n in data ]
        data = [ n.split( ) for n in data ]
        data = [ [ float( n ) for n in line if n != '' ] for line in data ]
        
        return np.array( data )
    
    def combine_data( self ):
        '''
        Combines discrete and smeared data into a single np.array
        '''
        histoData = self.load_csv( self.mainDirectory + self.histoFilename )
        if not self.newData:
            prevHistoData = self.load_csv( self.outDir + self.outFilename )
        
            # check that dimensions are the same
            if prevHistoData.shape[0] != histoData.shape[0]:
                print( "  Loaded histogram files are not of the same length. Check inputs!" )
                exit()
                
            return np.hstack(( prevHistoData, histoData[:,1][:,np.newaxis] ))
        
        return histoData[:,:2]
    
    def write_combined_file( self ):
        '''
        Write out new truncated .csv file
        '''
        data = self.combine_data()
        
        # write out mu data profile
        fullPath2File = self.outDir + self.outFilename
        print( '  Outputting file to {:s}'.format( fullPath2File ) )
        outfile = open( fullPath2File, 'w+' )
        for line in data:
            for el in line[:-1]:
                outfile.write( '{:0.6f} '.format( el ) )
            
            outfile.write( '{:0.6f}\n'.format( line[-1] ) )
            
        outfile.close()
        
        return 0
                
##############################################################################
# Execute script
##############################################################################

if __name__ == "__main__":    
    # --- Define path variables ---
    print( '*** Combining INDUS histogram data ***' )
    
    testing = 'False'
    if ( testing == 'True' ):
        histoName = 'histo_45.0.csv'
        outname = 'combined_histo.csv'
        workingDir = 'R:\\simulations\\isolated_sams_single\\homogeneous\\sam_8x8_300K_dodecanethiol_water\\indus\\umbrella\\indus_45.0\\'
        outDir = 'R:\\simulations\\isolated_sams_single\\homogeneous\\sam_8x8_300K_dodecanethiol_water\\indus\\wham\\'
        newData = False
        
    else:  
        # Adding options for command line input (e.g. --maxz, etc.)
        use = 'Usage: %prog [options]'
        parser = OptionParser(usage = use)
        parser.add_option( '-t', '--histo', dest = 'histoName', action = 'store', type = 'string', help = 'input file', default = '.' )
        parser.add_option( '-o', '--out', dest = 'outname', action = 'store', type = 'string', help = 'output file', default = '.' )
        parser.add_option( '-w', '--wd', dest = 'workingDir', action = 'store', type = 'string', help = 'working directory', default = '.' )
        parser.add_option( '-d', '--outdir', dest = 'outDir', action = 'store', type = 'string', help = 'output directory', default = '.' )
        parser.add_option( '-n', '--new', dest = 'newData', action = 'store', type = 'int', help = 'new data true or false', default = '1' )
        
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        # assign input parameters
        histoName = options.histoName
        outname = options.outname
        workingDir = options.workingDir
        outDir = options.outDir
        newData = bool(options.newData)
    
    indus_data = indus_histo_data( workingDir, outDir, histoName, outname, newData = newData )
    indus_data.write_combined_file()