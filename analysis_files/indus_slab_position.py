# -*- coding: utf-8 -*-
# NOCW
# This file is part of the INDUS project

R"""
The :class:`indus_cavity` determines the probe volume for an INDUS calculation
"""

##############################################################################
# Imports
##############################################################################
from __future__ import print_function, division
import platform
if platform.system() in [ 'Windows', 'Darwin' ]:
    import matplotlib.pyplot as plt
else:
    import matplotlib
    matplotlib.use('Agg') # turn off interactive plotting
    import matplotlib.pyplot as plt

# from willard_chandler import willard_chandler, density_field, wc_interface
from scipy.signal import argrelextrema # used to find first peak
from optparse import OptionParser # Used to allow commands within command line
# from parallel import parallel
from datetime import datetime
import mdtraj as md
import numpy as np
import time

##############################################################################
# indus_cavity class
##############################################################################
def indus_cavity( wc_file, wc_path, gro_file, xtc_file, traj_path, 
                  cavity_coords = [],
                  cavity_dimensions = '2.0,2.0,0.3',
                  num_waters = '-1', 
                  solvents = [ 'HOH', 'MET' ] 
                 ):
    R'''
    Determines properties of INDUS cavity
    INPUTS:
        wc_file: [string]
            name of wc_interface data file
        wc_path: [string]
            name of full path to wc_file directory
        gro_file: [string]
            name of gro data file            
        xtc_file: [string]
            name of xtc data file            
        traj_path: [string]
            name of full path to trajectory directory            
        cavity_coords: [list of floats, len = 3]
            list containing cavity xyz coords
        cavity_dimensions: [list of floats, len = 3]
            list containing cavity dimensions
        num_waters: [int]
            number of water in cavity
        solvents: [list of strings]
            list of strings to count in cavity
    OUTPUTS:

    '''
    # make full paths
    gro = traj_path + gro_file
    xtc = traj_path + xtc_file
    wc = wc_path + wc_file
    
    # split inputs
    if len(cavity_coords) > 0:
        cavity_coords = np.array([ float(el) for el in cavity_coords.split(',') ])
    
    if len(cavity_dimensions) > 0:
        cavity_dimensions = np.array([ float(el) for el in cavity_dimensions.split(',') ])
        
    num_waters = int(num_waters)
    
    # load trajectory
    traj = md.load( xtc, top = gro )
    traj = traj[-1000:]  # only use final 1 ns (assumes 1 ps sampling)
    
    # determine cavity properties
    properties = compute_cavity_properties( traj, wc, cavity_dimensions, num_waters, solvents )

    string = 'INDUS cavity coordinates: {:0.3f}, {:0.3f}, {:0.3f}'.format( properties['coords'][0],
                                                                           properties['coords'][1],
                                                                           properties['coords'][2] )
    write_txt( wc_path + 'cavity_coordinates.csv', string )
    
    string = 'INDUS cavity dimensions: {:0.3f}, {:0.3f}, {:0.3f}'.format( properties['dimensions'][0],
                                                                          properties['dimensions'][1],
                                                                          properties['dimensions'][2] )
    write_txt( wc_path + 'cavity_dimensions.csv', string )
    
    string = 'INDUS number of waters in slab: {:d}'.format( properties['num_waters'] )
    write_txt( wc_path + 'num_waters.csv', string )
    
def compute_cavity_properties( traj, wc_file, cavity_dimensions, num_waters, solvents ):
    R'''
    Determines properties to INDUS cavity
    INPUTS:
        traj: [mdtraj.trajectory]
            trajectory object from mdtraj
        wc_file: [string]
            full path name to wc_interface data file
        cavity_dimensions: [list of floats, len = 3]
            list containing cavity dimensions
        num_waters: [int]
            number of water in cavity
        solvents: [list of strings]
            list of strings to count in cavity
    OUTPUTS:
        properties: [dict]
            'coords': list of floats containing x,y,z coords
            'dimensions': list of floats containing cavity dimensions
            'num_water': int, number of waters in cavity
    '''
    # if number of waters has an input it takes priorty over sam dimensions
    if num_waters > 0:
        x_dim = cavity_dimensions[0]
        y_dim = cavity_dimensions[1]
        cavity_dimensions = []
        
    # input slab dimensions
    if len(cavity_dimensions) > 2:
        x_dim = cavity_dimensions[0]
        y_dim = cavity_dimensions[1]
        z_dim = cavity_dimensions[2]
    
    # get simulation box size
    box_vectors = traj.unitcell_lengths # np.array, nFrames x 3
        
    ## DETERMINE X- AND Y- POSITION FOR SAM POSITION
    ndx_water = np.array( [ atom.index for atom in traj.topology.atoms \
                           if atom.element.symbol == 'O' \
                           and atom.residue.name in [ 'HOH' ] ] ) # heavy water atoms
    
    ## FIND ALL CARBON ATOMS IN LIGANDS (assumes single component systems; for multi-component will only account for tilt)
    monolayer_atoms = [ [ atom.index for atom in residue.atoms \
                          if atom.element.symbol == 'C' ] \
                          for residue in traj.topology.residues \
                          if residue.name not in solvents ]
    last_carbon = np.array( [ ligand[-1] for ligand in monolayer_atoms ] )
    monolayer_position = traj.xyz[ :, last_carbon, : ]
    x_mono = np.mean( monolayer_position[:,:,0] )
    y_mono = np.mean( monolayer_position[:,:,1] )
    z_mono = np.mean( monolayer_position[:,:,2] )
    if x_mono < 0.0:
        x_mono += box_vectors[:,0].mean()

    if x_mono > box_vectors[:,0].mean():
        x_mono -= box_vectors[:,0].mean()
    
    if y_mono < 0.0:
        y_mono += box_vectors[:,1].mean()
        
    if y_mono > box_vectors[:,1].mean():
        y_mono += box_vectors[:,1].mean()

    ## DETERMINE Z FOR WILLARD-CHANDLER SURFACE
    wc_data = read_txt( wc_file )
    wc_data = wc_data[wc_data[:,2]<z_mono+1.0,:]

    dist_sq_center = ( wc_data[:,0] - x_mono )**2. + ( wc_data[:,1] - y_mono )**2.
    closest_ndx = np.nanargmin( dist_sq_center )            
    cavity_coords = np.array([ x_mono, y_mono, wc_data[closest_ndx,2] + 0.5 * z_dim ]) # shift z where base of cavity is on the corresponding x,y on the grid
    # cavity_coords = np.array([ x_mono, y_mono, data[:,0].mean() + 0.5 * z_dim ]) # shift z where base of cavity is on the avg contour    
 
    # determine num waters in cavity
    water_coords = traj.xyz[:,ndx_water,:]
    in_slab = np.all( np.logical_and( water_coords < cavity_coords + cavity_dimensions/2., 
                                      water_coords > cavity_coords - cavity_dimensions/2. ), axis = 2 )
    num_waters = in_slab.sum(axis=1).mean()
    num_waters = int( ( num_waters + 10 ) // 10 * 10 )
    
    # determine cavity dimensions, will need to update for fixed num waters
    cavity_dimensions = [ x_dim, y_dim, z_dim ]
    
    properties = { 'coords': cavity_coords, 'dimensions': cavity_dimensions, 'num_waters': num_waters }

    return properties

def read_txt( wc_file ):
    R'''
    Reads wc text file
    INPUT:
        wc_file: [string]
            full path name to wc data file
    OUTPUTS:
        data: [numpy.array, Nx3]
            array containing wc interface positions
    '''
    ## OPENING DATA FILE
    with open( wc_file ) as raw_data:
        data = raw_data.readlines()
    
    ## TRANSFORMING DATA TO NUMPY ARRAY
    data = np.array( [ [ float(el) for el in line.split(',') ] for line in data[2:] ] )
    
    return data

def write_txt( out_file, string ):
    R'''
    write out text file
    INPUT:
        file: [string]
            full path name to out data file
    OUTPUTS:
        data: [string]
            text to write to file
    '''
    print( string )
    
    outfile = open( out_file, 'w+' )
    outfile.write( string + '\n' )
    outfile.close()      
    
    print( 'written to {:s}\n'.format( out_file ) )      
    
##############################################################################
# Execute script
##############################################################################

if __name__ == "__main__":    
    print( '*** computing indus cavity properties ***' )
        
    # --- Define path variables ---    
    testing = 'False'
    if ( testing == 'True' ):
        args = { 'wc_file': 'sam_equil_willard_chandler.dat',
                 'wc_path': r'R:/simulations/polar_sams/indus/2x2x0.3nm/sam_single_12x12_300K_dodecanethiol_tip4p_nvt_CHARMM36_2x2x0.3nm/output_files/',
                 'gro_file': 'sam_equil_whole.gro',
                 'xtc_file': 'sam_equil_whole.xtc',
                 'traj_path':  r'R:/simulations/polar_sams/indus/2x2x0.3nm/sam_single_12x12_300K_dodecanethiol_tip4p_nvt_CHARMM36_2x2x0.3nm/equil/',
                 'cavity_coords': '',
                 'cavity_dimensions': '2.0,2.0,0.3',
                 'num_waters': '-1'
                }
    else:  
        # Adding options for command line input (e.g. --maxz, etc.)
        use = 'Usage: %prog [options]'
        parser = OptionParser(usage = use)
        parser.add_option( '-n', '--wc', dest = 'wc_file', action = 'store', type = 'string', help = 'wc input file', default = '.' )
        parser.add_option( '-p', '--wc_path', dest = 'wc_path', action = 'store', type = 'string', help = 'wc path', default = '.' )
        parser.add_option( '-g', '--gro', dest = 'gro_file', action = 'store', type = 'string', help = 'gro file', default = 'sam.gro' )
        parser.add_option( '-x', '--xtc', dest = 'xtc_file', action = 'store', type = 'string', help = 'xtc file', default = 'sam.xtc' )
        parser.add_option( '-t', '--traj_path', dest = 'traj_path', action = 'store', type = 'string', help = 'traj path', default = '.' )
        parser.add_option( '-c', '--coords', dest = 'coords', action = 'store', type = 'string', help = 'indus cavity coords', default = '' )
        parser.add_option( '-d', '--dimensions', dest = 'dimensions', action = 'store', type = 'string', help = 'indus cavity dimensions', default = '2.0,2.0,0.3' )
        parser.add_option( '-N', '--num_waters', dest = 'num_waters', action = 'store', type = 'string', help = 'number of waters in cavity', default = '-1' )

        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        args = { 'wc_file': options.wc_file,
                 'wc_path': options.wc_path,
                 'gro_file': options.gro_file,
                 'xtc_file': options.xtc_file,
                 'traj_path':  options.traj_path,
                 'cavity_coords': options.coords,
                 'cavity_dimensions': options.dimensions,
                 'num_waters': options.num_waters
                }

    indus_cavity( **args )
                        