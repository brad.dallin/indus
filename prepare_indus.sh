#!/bin/bash

# FLAGS FOR WHICH STEPS TO RUN
# Likely will have to run this script multiple times, hence importance of flags
# Note that variables needed to run each section of the script are defined in the relevant section - maybe undo this?

# check simulation details
bash ./check_prep_details.sh

# --- Load simulation details ---
echo "Loading simulation system details from prepare_indus_details.sh"
source $HOME/indus/indus_rc.sh
source ./indus_prep_details.sh
source /app/gromacs_2016/bin/GMXRC

# --- Define repeated files and paths ---
# set path variables
path_mdp="$PATH_PREP_SCRIPTS/mdp_files/$FORCEFIELD"
path_plumed="$PATH_PREP_SCRIPTS/plumed_files"
path_run="$PATH_PREP_SCRIPTS/run_files"
py_prep_file="$PATH_MDBUILDER/builder/make_planar_sam.py"
py_add_position_restraint="$PATH_MDBUILDER/core/add_position_restraint.py"

# Input/Output folders
input_folder=input_files   # includes topology, mdp files, itp files, etc. 
output_folder=output_files # includes final trajectory files, analysis files, etc.

ffName=$( echo ${FORCEFIELD%-*} | tr [:lower:] [:upper:] )

ligand1=$( echo "$SAM_LIGANDS" | cut -f1 -d, )
ligand2=$( echo "$SAM_LIGANDS" | cut -f2 -d, )

fract1=$( echo "$LIGAND_FRACTION" | cut -f1 -d, )
fract2=$( echo "$LIGAND_FRACTION" | cut -f2 -d, )

# --- Build simulation systems ---
if [ $SAM_TYPE == "single" ]; then
    # solvent in system
    solvent=${WATER_MODEL}
    if [[ $COSOLVENT != "none" && $(awk "BEGIN{ if ($COSOLVENT_FRACTION > 0.0) print 1; else print 0}") -eq 1 ]]; then
        solvent=${COSOLVENT}
        if [ $(awk "BEGIN{ if ($COSOLVENT_FRACTION < 1.0) print 1; else print 0}") -eq 1 ]; then
            solventFraction=$(awk "BEGIN{ print 1 - $COSOLVENT_FRACTION }")
            solvent=${WATER_MODEL}${solventFraction}_${COSOLVENT}${COSOLVENT_FRACTION}
        fi
    fi

    sim_name=sam
    if [ $ligand1 == $ligand2 ]; then
        # monolayer is homogeneous (one type of ligand)
        if [ $PATTERN == "random" ]; then
            wd=$OUT_FOLDER/sam_single_${NUM_LIG_X}x${NUM_LIG_Y}_${SIM_TEMPERATURE}K_${ligand1}_${solvent}_${ENSEMBLE}_${ffName}
        else
            wd=$OUT_FOLDER/sam_single_${NUM_LIG_X}x${NUM_LIG_Y}_${PATTERN}_${SIM_TEMPERATURE}K_${ligand1}_${solvent}_${ENSEMBLE}_${ffName}
        fi
    else
        # monolayer is heterogeneous (two types of LIGANDS)
        if [ $PATTERN == "random" ]; then
            wd=$OUT_FOLDER/sam_single_${NUM_LIG_X}x${NUM_LIG_Y}_${SIM_TEMPERATURE}K_${ligand1}${fract1}_${ligand2}${fract2}_${solvent}_${ENSEMBLE}_${ffName}
        else
            wd=$OUT_FOLDER/sam_single_${NUM_LIG_X}x${NUM_LIG_Y}_${PATTERN}_${SIM_TEMPERATURE}K_${ligand1}${fract1}_${ligand2}${fract2}_${solvent}_${ENSEMBLE}_${ffName}
        fi
    fi
    
    # define GROMACS input file names
    topFile=${sim_name}.top
    groFile=${sim_name}.gro

    # Create simulation input/output folders and move into simulation working directory
    echo "  Creating $wd/$input_folder"
    echo "  Creating $wd/$output_folder"
    mkdir -p $wd/$input_folder
    mkdir -p $wd/$output_folder
    cd $wd

    # Copying force field/mdp files to working directory
    echo "  Copying $PATH_FF/$FORCEFIELD to $wd/$FORCEFIELD"
    cp -rv $PATH_FF/$FORCEFIELD ./$FORCEFIELD
    cp -v $PATH_LIGANDS/$FORCEFIELD/$ligand1/$ligand1.itp ./$FORCEFIELD/$ligand1.itp
    cp -v $PATH_LIGANDS/$FORCEFIELD/$ligand2/$ligand2.itp ./$FORCEFIELD/$ligand2.itp
    if [ $ffName == "CHARMM36" ]; then
        cp -v $PATH_LIGANDS/$FORCEFIELD/$ligand1/$ligand1.prm ./$FORCEFIELD/$ligand1.prm
        cp -v $PATH_LIGANDS/$FORCEFIELD/$ligand2/$ligand2.prm ./$FORCEFIELD/$ligand2.prm
    fi
    cp -v $path_mdp/minim.mdp ./$input_folder/
    if [[ $PATTERN == 'checker' || $PATTERN == 'wide_checker' || $PATTERN == 'janus' || $PATTERN == 'single' ]]; then
        pattern_file=$PATH_PATTERNS/${PATTERN}_${NUM_LIG_X}x${NUM_LIG_Y}_${fract1}.dat
    else
        pattern_file="None"
    fi
    
    # run python script here with flags for above input files (this is where to include golds)
    python $py_prep_file --ogro $groFile --otop $topFile --ofold $wd --ff $FORCEFIELD --prep $PATH_LIGANDS/$FORCEFIELD --ligx $NUM_LIG_X --ligy $NUM_LIG_Y --names $SAM_LIGANDS --fracs $LIGAND_FRACTION --single True --nlayers $NUM_LAYERS --gold $INCLUDE_GOLD --vir $VIRTUAL_SITES --pol $POLARIZATION --int $INTERFACEFF --pattern $pattern_file
    python $py_add_position_restraint --ifold $wd/$FORCEFIELD --names $SAM_LIGANDS
    
    # get current box vectors
    array=($(tail -n 1 $groFile))
    xBox=${array[0]}
    yBox=${array[1]}
    zBoxMono=${array[2]}
    echo "Current box vectors: $xBox $yBox $zBoxMono"

    echo "Creating solvent layers"
    # Get solvent volumes (z is only variable where x,y are set by monolayer x,y dims)
    zBoxCosolvent=$(awk "BEGIN {print $COSOLVENT_FRACTION * $SOLVENT_HEIGHT; exit}")
    zBoxWater=$(awk "BEGIN {print $SOLVENT_HEIGHT - $zBoxCosolvent; exit}")
    # Find box z dimension for monolayers plus solvents
    zNew=$(awk "BEGIN{ print $zBoxMono + $SOLVENT_HEIGHT + 0.5; exit}")

    # increase z dimension of box, DON'T CENTER THE MONOLAYERS
    gmx editconf -f $groFile -o ${sim_name}_resize.gro -box $xBox $yBox $zNew -noc &>/dev/null
    boxVector=$(tail -n 1 ${sim_name}_resize.gro)
    # find number of atoms in box
    nMonoAtoms=$(head -n 2 ${sim_name}_resize.gro | tail -n 1)
    # remove whitespace
    nMonoTrim="${nMonoAtoms%"${nMonoAtoms##*[![:space:]]}"}"
    nMonoTrim="${nMonoTrim#"${nMonoTrim%%[![:space:]]*}"}"
    monoOut="monolayer atoms = $nMonoTrim ;"
    waterOut=""
    cosolventOut=""
    # make a new file to concatenate
    cp -v ${sim_name}_resize.gro ${sim_name}_add.gro  

    # default number of solvent atoms is 0
    nWaterTrim=0
    nCosolventTrim=0
    # Layer solvent (this may be useful for implement multiple solvents)
    ### Make a water layer ###
    if [ $(awk "BEGIN{ if ($zBoxWater > 0.0) print 1; else print 0}") -eq 1 ]; then
        echo "  Creating a water layer"
        echo "  Water box vectors: $xBox $yBox $zBoxWater"
        # make new box of required size and fill with spce water molecules
        if [[ $WATER_MODEL == 'spce' || $WATER_MODEL == 'SPCE' || $WATER_MODEL == 'tip3p' || $WATER_MODEL == 'TIP3P' ]]; then
            gmx solvate -cs spc216.gro -o water_layer.gro -box $xBox $yBox $zBoxWater &>/dev/null
        fi

        if [[ $WATER_MODEL == 'tip4p' || $WATER_MODEL == 'TIP4P' ]]; then
            gmx solvate -cs tip4p.gro -o water_layer.gro -box $xBox $yBox $zBoxWater &>/dev/null
        fi

        if [[ $WATER_MODEL == 'tip5p' || $WATER_MODEL == 'TIP5P' ]]; then
            gmx solvate -cs tip5p.gro -o water_layer.gro -box $xBox $yBox $zBoxWater &>/dev/null
        fi
        # resize water box, translate to appropriate position relative to monolayers
        zShift=$(awk "BEGIN{ print $zBoxMono + 0.2}")
        gmx editconf -f water_layer.gro -o water_layer_resize.gro -box $xBox $yBox $zNew -noc -translate 0 0 $zShift &>/dev/null
        # find number of atoms in box
        nWaterAtoms=$(head -n 2 water_layer_resize.gro | tail -n 1)
        # remove whitespace
        nWaterTrim="${nWaterAtoms%"${nWaterAtoms##*[![:space:]]}"}"
        nWaterTrim="${nWaterTrim#"${nWaterTrim%%[![:space:]]*}"}"
        waterOut="water atoms = $nWaterTrim ;"

        # adjust gro file and concatenate
        sed -i '1d' water_layer_resize.gro
        sed -i '1d' water_layer_resize.gro
        sed -i '$d' water_layer_resize.gro
        sed -i '$d' ${sim_name}_add.gro
        cat ${sim_name}_add.gro water_layer_resize.gro > temp.gro
        mv temp.gro ${sim_name}_add.gro
        echo "$boxVector" >> ${sim_name}_add.gro

        if [ $ffName == 'OPLS' ]; then
            if [[ $WATER_MODEL == 'tip3p' || $WATER_MODEL == 'TIP3P' ]]; then
                cp -v $topFile ${topFile}.backup
                nWaterMolecules=$(($nWaterTrim/3))
                echo " SOL    $nWaterMolecules" >> $topFile
            fi

            if [[ $WATER_MODEL == 'spce' || $WATER_MODEL == 'SPCE' ]]; then
                sed -i 0,/tip3p-mod/s/tip3p-mod/spce/ $topFile # if spce instead of tip3p
                cp -v $topFile ${topFile}.backup
                nWaterMolecules=$(($nWaterTrim/3))
                echo " SOL    $nWaterMolecules" >> $topFile
            fi

            if [[ $WATER_MODEL == 'tip4p' || $WATER_MODEL == 'TIP4P' ]]; then
                sed -i 0,/tip3p-mod/s/tip3p-mod/tip4p/ $topFile # if tip4p instead of tip3p
                cp -v $topFile ${topFile}.backup
                nWaterMolecules=$(($nWaterTrim/4))
                echo " SOL    $nWaterMolecules" >> $topFile
            fi

            if [[ $WATER_MODEL == 'tip5p' || $WATER_MODEL == 'TIP5P' ]]; then
                sed -i 0,/tip3p-mod/s/tip3p-mod/tip5p/ $topFile # if tip5p instead of tip3p
                cp -v $topFile ${topFile}.backup
                nWaterMolecules=$(($nWaterTrim/5))
                echo " SOL    $nWaterMolecules" >> $topFile
            fi
        fi

        if [ $ffName == 'CHARMM36' ]; then
            if [[ $WATER_MODEL == 'tip3p' || $WATER_MODEL == 'TIP3P' ]]; then
                cp -v $topFile ${topFile}.backup
                nWaterMolecules=$(($nWaterTrim/3))
                echo " SOL    $nWaterMolecules" >> $topFile
            fi

            if [[ $WATER_MODEL == 'spce' || $WATER_MODEL == 'SPCE' ]]; then
                sed -i 0,/tip3p/s/tip3p/spce/ $topFile # if spce instead of tip3p
                cp -v $topFile ${topFile}.backup
                nWaterMolecules=$(($nWaterTrim/3))
                echo " SOL    $nWaterMolecules" >> $topFile
            fi

            if [[ $WATER_MODEL == 'tip4p' || $WATER_MODEL == 'TIP4P' ]]; then
                sed -i 0,/tip3p/s/tip3p/tip4p/ $topFile # if tip4p instead of tip3p
                cp -v $topFile ${topFile}.backup
                nWaterMolecules=$(($nWaterTrim/4))
                echo " SOL    $nWaterMolecules" >> $topFile
            fi

            if [[ $WATER_MODEL == 'tip5p' || $WATER_MODEL == 'TIP5P' ]]; then
                sed -i 0,/tip3p/s/tip3p/tip5p/ $topFile # if tip5p instead of tip3p
                cp -v $topFile ${topFile}.backup
                nWaterMolecules=$(($nWaterTrim/5))
                echo " SOL    $nWaterMolecules" >> $topFile
            fi        
        fi
    fi

    ### Make a COSOLVENT layer ###
    if [ $(awk "BEGIN{ if ($zBoxCosolvent > 0.0) print 1; else print 0}") -eq 1 ]; then
        echo "  Creating a $COSOLVENT layer"
        echo "  COSOLVENT box vectors: $xBox $yBox $zBoxCosolvent"
        # make new box of required size and fill with spce water molecules
        gmx solvate -cs $pathScripts/solvents/${COSOLVENT}_equil.gro -o ${COSOLVENT}_layer.gro -box $xBox $yBox $zBoxCosolvent &>/dev/null
        # resize COSOLVENT box, translate to appropriate position relative to monolayers, DON'T CENTER
        zShift=$(awk "BEGIN{ print $zBoxMono + $zBoxWater + 0.2}")
        gmx editconf -f ${COSOLVENT}_layer.gro -o ${COSOLVENT}_layer_resize.gro -box $xBox $yBox $zNew -noc -translate 0 0 $zShift &>/dev/null
        # find number of atoms in box
        nCosolventAtoms=$(head -n 2 ${COSOLVENT}_layer_resize.gro | tail -n 1)
        # remove whitespace
        nCosolventTrim="${nCosolventAtoms%"${nCosolventAtoms##*[![:space:]]}"}"
        nCosolventTrim="${nCosolventTrim#"${nCosolventTrim%%[![:space:]]*}"}"
        cosolventOut="$COSOLVENT atoms = $nCosolventTrim ;"

        # adjust gro file and concatenate
        sed -i '1d' ${COSOLVENT}_layer_resize.gro
        sed -i '1d' ${COSOLVENT}_layer_resize.gro
        sed -i '$d' ${COSOLVENT}_layer_resize.gro
        sed -i '$d' ${sim_name}_add.gro
        cat ${sim_name}_add.gro ${COSOLVENT}_layer_resize.gro > temp.gro
        mv temp.gro ${sim_name}_add.gro
        echo "$boxVector" >> ${sim_name}_add.gro

        # Find COSOLVENT resname
        resname=$(awk '{print $2}' FIELDWIDTHS="5 5 5 5 8 8 8" ${COSOLVENT}_layer_resize.gro | awk 'FNR == 3 {print}')
        resname="${resname%"${resname##*[![:space:]]}"}" # remove trailing whitespace
        # number of atoms in a single COSOLVENT molecule
        n=$(awk '$1=="    1" {print $1}' FIELDWIDTHS="5 5 5 5 8 8 8" ${COSOLVENT}_layer_resize.gro | awk '{print NR}' | tail -1)
        # update topology file
        echo "  Updating topology file"
        cp -v $topFile ${topFile}.backup
        nCosolventMolecules=$(($nCosolventTrim/$n))
        echo " $resname    $nCosolventMolecules" >> $topFile
    fi

    # Add box vector as last line
    echo "  New box z vector: $zNew"
    # determine final number of atoms
    nTotalAtoms=$(($nMonoTrim + $nWaterTrim + $nCosolventTrim))
    echo "  monolayers and solvent layers combined"
    echo "  final box vectors: $xBox $yBox $zNew"    
    echo "  $monoOut $waterOut $cosolventOut Total atoms = $nTotalAtoms"
    sleep 2

    # replace number of atoms with combined number of atoms
    sed -i -e 0,/$nMonoTrim/s/$nMonoTrim/$nTotalAtoms/ ${sim_name}_add.gro
    # renumber atoms in gro file
    gmx genconf -f ${sim_name}_add.gro -o ${sim_name}_add.gro -renumber &>/dev/null
    gmx editconf -f ${sim_name}_add.gro -o ${sim_name}_add.gro -box $xBox $yBox $(awk "BEGIN{ print $zNew + 1.5 }") &>/dev/null

    # clean up unnecessary files
    rm *_layer.gro
    rm *_resize.gro
    rm ${sim_name}.gro

    # Prepare/Run energy minimization
    gmx grompp -quiet -f $input_folder/minim.mdp -c ${sim_name}_add.gro -p $topFile -o ${sim_name}_em.tpr
    # neutralize if necessary here using tpr file just generated
    gmx genion -s ${sim_name}_em.tpr -p $topFile -o ${sim_name}_add.gro -neutral << INPUT
SOL
INPUT

    # remake tpr file and minimize
    gmx grompp -maxwarn 2 -f $input_folder/minim.mdp -c ${sim_name}_add.gro -p $topFile -o ${sim_name}_em.tpr
    gmx mdrun -quiet -nt 4 -deffnm ${sim_name}_em

    # clean up directory by removing unnessary files
    mv ${sim_name}_em.gro $input_folder/${sim_name}.gro
    mv $topFile $input_folder/
    rm -f \#*\#
    rm -f *.pdb
    rm *.backup
    rm mdout.mdp
    rm *_add.gro
    rm *_em*

    echo "***  single SAM preparation completed  ***"
    echo "***  Ready to run MD simulation  ***"
    echo ""
    echo ""
    sleep 2

    echo "  Directory name changed to ${wd}_2x2x0.3nm"
    if [ ! -e ${wd}_2x2x0.3nm ]; then
        mv -v $wd ${wd}_2x2x0.3nm
        wd=${wd}_2x2x0.3nm
    else
        n=1
        while [ -e ${wd}_2x2x0.3nm_$n ]; do
            n=$(( $n + 1 ))
        done

        echo ""
        echo "***  ${wd}_2x2x0.3nm exists!! Moving files into ${wd}_2x2x0.3nm_$n***"
        echo ""
        sleep 5
        mv -v $wd ${wd}_2x2x0.3nm_$n
        wd=${wd}_2x2x0.3nm_$n
    fi

    cd $wd/
    # Copying plumed files to simulation input directory
    echo "  Copying $path_mdp/ to $wd/$input_folder/"
    echo "  Copying $path_plumed/ to $wd/$input_folder/"
    echo "  Copying $path_run/indus_details.sh to $wd/"
    cp -v $path_mdp/* ./$input_folder/
    cp -v $path_plumed/* ./$input_folder/
    cp -v $path_run/indus_details.sh ./

    # update simulation run script
    echo "  Copying $path_run/run_indus.sh to $wd/"
    echo "  Updating run_indus.sh with proper run parameters"
    cp -v $path_run/run_indus.sh ./
    sed -i s/SIMNAME/$sim_name/g ./run_indus.sh
    sed -i s/PULLRATE/$PULL_RATE/g ./run_indus.sh
    sed -i s/SIMULATIONTIME/$SIMULATION_TIME/g ./run_indus.sh
    sed -i s/SIMULATIONTEMP/$SIM_TEMPERATURE/g ./run_indus.sh
    sed -i s/FORCEFIELD/$FORCEFIELD/g ./run_indus.sh
    sed -i s/NUMMPI/$NUM_MPI/g ./run_indus.sh
    sed -i s/NUMOMP/$NUM_OMP/g ./run_indus.sh

    # update multi simulation run script
    echo "  Copying $path_run/run_indus.sh to $wd/"
    echo "  Updating run_indus.sh with proper run parameters"
    cp -v $path_run/run_indus_multi.sh ./
    sed -i s/SIMNAME/$sim_name/g ./run_indus_multi.sh
    sed -i s/PULLRATE/$PULL_RATE/g ./run_indus_multi.sh
    sed -i s/SIMULATIONTIME/$SIMULATION_TIME/g ./run_indus_multi.sh
    sed -i s/SIMULATIONTEMP/$SIM_TEMPERATURE/g ./run_indus_multi.sh
    sed -i s/FORCEFIELD/$FORCEFIELD/g ./run_indus_multi.sh
    sed -i s/NUMMPI/$NUM_MPI/g ./run_indus_multi.sh
    sed -i s/NUMOMP/$NUM_OMP/g ./run_indus_multi.sh
    sed -i s/NUMPERWINDOW/$NUM_PER_WINDOW/g ./run_indus_multi.sh

    # update submit script
    echo ""
    echo "  Copying $path_run/submit_${SERVER}.sh to $wd/"
    echo "  Updating submit_${SERVER}.sh with proper parameters"
    cp -v $path_run/submit_${SERVER}.sh ./ # would need to change for other servers
    sed -i s/JOBNAME/$JOB_NAME/g ./submit_${SERVER}.sh
    sed -i s/RUNSCRIPT/run_indus.sh/g ./submit_${SERVER}.sh

    echo "file location $wd"
fi
