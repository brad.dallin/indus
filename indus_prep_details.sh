#!/bin/bash

# List of simulation details to be specified for an MD simulation

# --- Simulation system ---
# you must include a temperature distribuiton file in ~/hydrophobicity_project/input_files/remd_files
# FUTURE update automate temperature distribution generation
PREPARE_SYSTEM=1                   # set to 1 to prepare system
ENSEMBLE=nvt                       # select simulation ensemble (npt or nvt)
FORCEFIELD=charmm36-jul2017.ff     # select force field for simulation (charmm36-jul2017.ff only one working for now, opls-aa coming soon?)
SIM_TEMPERATURE=300                # temperature to run initial equilibration (should be temp of interest for REMD)

# solvent details
WATER_MODEL=tip4p                  # choose water model (SPCE, TIP3P, or TIP4P)
COSOLVENT=none                     # select cosolvent (methanol available currently)
COSOLVENT_FRACTION=0.0             # volume fraction of cosolvent in water
SOLVENT_HEIGHT=5.0                 # volume of water above top monolayer (x,y dimensions are determined by numLigx,numLigy)

# sam details
SAM_TYPE=single                                         # type of sam system (single or double)
SAM_LIGANDS=dodecanethiol,C13OH                         # name of ligands in monolayer for mixed system separate by a comma (e.g. ligand1,ligand2)
LIGAND_FRACTION=0.625,0.375                                # fraction of ligands in monolayer mixed system separate by a comma (e.g. comp1,comp2) (must sum to MOLFRACT)
PATTERN=janus                                          # Patterned for SAM (PATTTYPE, PATTTYPE, or PATTTYPE)
LAYER_SEPARATION=1.0701                                # Estimated from Au-S bond (separation between sulfur layers)
NUM_LIG_X=8                                            # number of row ligands
NUM_LIG_Y=8                                            # number of column ligands
INCLUDE_GOLD=False                                      # set to 1 to include gold atoms (not implemented yet)
NUM_LAYERS=0                                            # number of gold layers
VIRTUAL_SITES=False                                     # virtual gold sites
POLARIZATION=False                                      # polarizable gold
INTERFACEFF=False                                       # interface FF for gold

# --- Simulation type and details ---
OUT_FOLDER=$HOME/simulations/polar_sams
SERVER=swarm                          # which server will job be submitted
JOB_NAME=unb_60C13OH                  # batch job name
SIMULATION_TIME=5                     # simulation time (ns)

# indus details
PULL_RATE=1000                        # pull rate (nsteps/water molecule)

# Technical flags (Optimized for swarm)
NUM_MPI=4                             # number of mpi 
NUM_OMP=7                             # number of open mp threads
NUM_PER_WINDOW=2                      # number of cores per window (if multi)
