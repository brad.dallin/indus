# -*- coding: utf-8 -*-
"""
make_planar_sam.py
The purpose of this script is to make planar sams as a monolayer

CREATED ON: 05/04/2018

AUTHOR(S):
    Alex K. Chew (alexkchew@gmail.com)

INPUTS:
    - force field
    - preparation files
    - layer separation between the SAMs?
    
OUTPUTS:
    - gro file
    - topology file
    
ALGORITHM:
    - load in the ligands
    - create planar monolayer (bottom and top)
    - load in the gold planar lattice (replicate layers)
    - move planar monolayer accordingly
    - output gro file
    - output topology file
    
** UPDATES **
2018-05-04: Added function to increase gold thickness
2018-06-26: Updating default gold layer thickness: 0.4709 nm
"""
### IMPORTING MODULES
import sys
if r'R:/bin/python_modules' not in sys.path:
    sys.path.append(r'R:/bin/python_modules')
import numpy as np # Mathematical tool for arrays, etc.
from optparse import OptionParser # Used to allow commands within command line
from random import shuffle # Helps shuffle random numbers
from MDBuilder.applications.nanoparticle.global_vars import GOLD_DIAMETER_NM
## CHECK PATH FUNCTION
from MDBuilder.core.check_tools import check_server_path ## CHECK PATHS
from MDBuilder.core.check_tools import check_testing ## CHECKING PATH FOR TESTING
## PARSER OPTIONS
from MDBuilder.builder.ligand_builder import get_ligand_args, Ligand
## FORCEFIELD
from MDBuilder.core.forcefield import FORCEFIELD
## EXPORTING TOOLS
from MDBuilder.core.export_tools import print_gro_file_combined_res_atom_names

### GLOBAL VARIABLES
from MDBuilder.applications.nanoparticle.global_vars import GOLD_SULFUR_CUTOFF, SULFUR_ATOM_NAME
from MDBuilder.applications.nanoparticle.global_vars import PLANARGOLDITPNAME as GOLDITPNAME

############################## MONOLAYER CLASS ##############################
class Monolayer:
    '''
    builds sqrt 3 x sqrt3 R30 hexagonal overlayer superimposed over gold lattice
    It's just hexagonally close packed with 3x the spacing, basically.
    Definitely a simplification given more recent data on superstuctures
    Orientation is either +1 or -1, and determines z-direction. trans_z
    is added directly to the z-coordinate as well.
    INPUTS:
        forcefield: Force field from forcefield class
        num_lig_x: Number of ligands in the x-direction
        num_lig_y: Number of ligands in the y-direction
        orientation: +1 for pointing up, -1 for pointing down
        trans_z: Layer separation in nm between the two layers
        ligands: Ligand class as a list
        ligand_nums: Number of ligands in each list entry
    OUTPUTS:
        self.monolayer_geom: [np.array, shape=(N,3)] geometry of the monolayer
        self.monolayer_atomnames: [list] atomnames for the monolayer
        self.monolayer_resids: [np.array, shape=(N,1)] residue IDs for the monolayer
        self.monolayer_outputnames: [list] outputnames for the monolayer
        self.num_monolayer_atoms: [int] total number of monolayer atoms
    '''
    
    # Initializes monolayer with num_lig_x by num_lig_y ligands, oriented in either +1 or -1 direction, offset_y
    # vertically by trans_z, with the list of ligand names denoting the ligands randomly distributed in the monolayer.
    def __init__(self, num_lig_x, num_lig_y, orientation, trans_z, ligands, ligand_nums, pattern_file):
        
        ## LOOPING THROUGH LIGANDS -- ENSURING THAT GEOMETRY OF THE LIGAND STARTS WITH SULFUR AT (0,0,0)
        ## LIGANDS SHOULD BE DIRECTIONALLY POINTING UPWARD IN THE Z-DIRECTION
        ## RE-CENTERING LIGAND SO SULFUR IS AT ZERO
        for current_lig in ligands:
            if current_lig:
                # FINDING SULFUR INDEX
                sulfurIndex = current_lig.ligand_atomnames.index(SULFUR_ATOM_NAME) # Find sulfur index ** FIRST INDEX **
                ## DEFINING LIGAND GEOMETRY
                lig_geometry = current_lig.ligand_geom[:] - current_lig.ligand_geom[sulfurIndex]
                ## STORING IN LIGANDS
                current_lig.ligand_geom = lig_geometry[:]
        
        # Create list of all ligand objects, then shuffle to get random positions
        ligand_objects = []
        
        if pattern_file == "None":
            for i in range(0, len(ligands)):
                cur_lig = ligands[i]
                for j in range(0, int(ligand_nums[i])):
                    ligand_objects.append(cur_lig) # Appends different ligands with respect to the number of ligand (e.g. butanethiol, butanethiol, decanethiol, etc.)
            # Randomize list
            shuffle(ligand_objects)
        else:
            ligand_objects = [0]*(num_lig_x*num_lig_y)
            pattern = self.read_pattern_file(pattern_file)
            for i in range(0, len(ligands)):
                cur_lig = ligands[i]
                for j in range(0, len(pattern)):
                    if (int(pattern[j]) == i):
                        ligand_objects[j] = (cur_lig) # Adds different ligands with respect to the given pattern        
        
        # generate geometry by combining ligand geometries
        self.monolayer_geom = np.zeros((0, 3), dtype='float')
        # make list of atomnames
        self.monolayer_atomnames = []
        # make list of resids
        self.monolayer_resids = np.zeros((0, 1), dtype='int')
        # list of all ligand output naems for gro file
        self.monolayer_outputnames = []

        # choose geometric parameters based on xsqrt(3) packing for AU (111) plane
        sam_incr_x = 1.5*GOLD_DIAMETER_NM # distance between columns
        sam_incr_y = 1.5*np.tan(np.pi/6)*2*GOLD_DIAMETER_NM # 1.5 diameters * tan 30 degrees * 2 <-- Point far away, if ligand = 2, add this, if xligand = 1, add half of this
        offset_x = 0.5*GOLD_DIAMETER_NM # D/2 <-- Offset assuming gold at center
        offset_y = 0.5*GOLD_DIAMETER_NM*np.tan(np.pi/6) # D / 2 tan 30 <-- Assumes gold at (0,0)
        # first build top layer
        ligand_counter = 1
    
        # Add ligands to monolayer at random geometric positions, but list so that
        # we have blocks of ligands in gro/itp file. A little bit complicated
        # but massively simplifies the output files for gromacs
        for lig in ligands:
            if lig:
                for lig_x in range(0,num_lig_x):
                    for lig_y in range(0,num_lig_y):
                        cur_ligand = ligand_objects[lig_x*num_lig_y + lig_y]
                        # Add to list if this ligand is of correct type, so that
                        # we have blocks of ligands in output eventually.
                        if (cur_ligand == lig):
                            # Define geometry of monolayer based on presumed gold positions
                            trans_x = lig_x*sam_incr_x + offset_x
                            # here we change the offset in alternating rows to achieve hexagonal lattice
                            if (lig_x % 2 == 0):
                                trans_y = lig_y*sam_incr_y + offset_y + 0.5*sam_incr_y
                            else:
                                trans_y = lig_y*sam_incr_y + offset_y
                            # multiply all z-dimension geometry by orientation, first
                            lig_geom_orient = cur_ligand.ligand_geom * [1, 1, orientation]
                            # add ligand geometry translated by this amount
                            lig_geom_trans = lig_geom_orient + [trans_x, trans_y, trans_z]
                            # add ligand geometry to monolayer geometry
                            self.monolayer_geom = np.append(self.monolayer_geom, lig_geom_trans, axis=0)
                            # add atomnames as well
                            self.monolayer_atomnames.extend(cur_ligand.ligand_atomnames)
                            # assign resid for each of these atoms
                            self.monolayer_resids = np.append(self.monolayer_resids, ligand_counter*np.ones(cur_ligand.ligand_num_atoms))
                            self.monolayer_outputnames.extend(cur_ligand.ligand_outputnames)
                            ligand_counter += 1
        #self.build_monolayer(num_lig_x, num_lig_y, cur_ligand, orientation, trans_z)
        self.num_monolayer_atoms = np.shape(self.monolayer_geom)[0]
    
    @staticmethod
    def read_pattern_file(filename):
        '''
        Convert file patter into a 2D list
        '''
        pattern = open(filename, 'r')
        lines = pattern.readlines()
        numLines = len(lines)

        # Strip \n
        for i in range(0, numLines):
            lines[i] = lines[i].rstrip()
            
        # Convert rows in lines from str to list
        for i in range(0, numLines):
            lines[i] = lines[i].split()
                
        pattern.close()
    
        ligand_pattern = [0]*(numLines*len(lines[i]))
    
        for i in range(0,numLines*len(lines[i])):
            len_row = len(lines[i])
            print( len_row )
            ligand_pattern[i] = lines[i//numLines][i%8]
            
        return(ligand_pattern)
    
    @staticmethod
    def print_top_file(forcefield, output_top, output_folder, num_lig_x, num_lig_y, ligand_names, ligands, ligand_nums, wantGold, GOLDITPNAME, gold_resname, wantSingle):
        '''
        The purpose of this script is to create a topology file
        INPUTS:
            forcefield: forcefield class
            output_top : topology name
            output_folder: folder for topology
            num_lig_x: Number of ligands in the x-direction
            num_lig_y: Number of ligands in the y-direction
            ligand_names: Names of the ligands (list)
            ligands: Ligand class
            ligand_nums: Numbers for the ligands
            wantGold: True if you want gold
            GOLDITPNAME: Name of the gold itp file
            gold_resname: Name of the residue of gold
        OUTPUTS:
            TOPOLOGY FILE
        '''
        output_top_name = output_folder + '/' + output_top
        outputfile = open(output_top_name, "w")
        print("Outputting %s"%(output_top_name))
        # write basic topology information here
        outputfile.write(";  Topology file for %d x %d SAM\n\n"%(num_lig_x, num_lig_y))
        # include statements
        outputfile.write("#include \"%s\"\n"%(forcefield.forcefield_itp))
        if wantGold==True:
            outputfile.write("#include \"%s\"\n"%(forcefield.forcefield_folder + '/' + GOLDITPNAME + ".itp"))
        # include all ligand names (.itp and .prm, if charmm36)
        if "charmm36" in forcefield.forcefield_folder:
            for ligname in ligand_names:
                if ligname != 'none':
                    outputfile.write("#include \"%s.prm\"\n"%(forcefield.forcefield_folder + '/' + ligname))
                                 
        for ligname in ligand_names:
            if ligname != 'none':
                outputfile.write("#include \"%s.itp\"\n"%(forcefield.forcefield_folder + '/' + ligname))
                                 
        #outputfile.write("#include \"%s\"\n"%(GOLD_ITP_NAME))
        # add water here, although the molecules are added separately
        outputfile.write("#include \"%s\"\n"%(forcefield.forcefield_ions))
        outputfile.write("#include \"%s\"\n"%(forcefield.forcefield_water))
        outputfile.write("#include \"%s\"\n"%(forcefield.forcefield_methanol))
        outputfile.write("\n[ system ]\n")
        outputfile.write("frame t=1.000 in water\n")
        outputfile.write("\n[ molecules ] \n")
        outputfile.write("; compound   nr_mol\n")
        # output for each ligand in order
        if wantSingle:
            for i in range(0, len(ligand_names)):
                if ligands[i]:
                    lig=ligands[i]
                    outputfile.write(" %s     %d\n"%(lig.ligand_res_name, ligand_nums[i]))
        else:
            for j in range(0, 2):
                for i in range(0, len(ligand_names)):
                    if ligands[i]:
                        lig=ligands[i]
                        outputfile.write(" %s     %d\n"%(lig.ligand_res_name, ligand_nums[i]))
                    
        if wantGold == True:
            # Inputting gold atoms
            outputfile.write(" %s     %d\n"%(gold_resname, 1))
        
        outputfile.close()
        

#%% MAIN SCRIPT
if __name__ == "__main__":

    # Send user some output
    print('')
    print('**** RUNNING LIGAND BUILDER ****')

    # Turn test on or off
    testing = check_testing() # False if you're running this script on command prompt!!!

    ### CHECK IF YOU ARE TESTING
    if testing == True:
        # *-*- Testing parameters *-*- #
        ### DEFINING DIRECTORY STRUCTURE
        prep_folder=check_server_path(r"R:\hydrophobicity_project\ligands\OPLS")
        
        # ligand names to load in ligand_builder; see options in ligands folder
#        ligand_names=["butanethiol", "decanethiol"]
        ligand_names=["dodecanethiol"]
        # specifies ligand fractions in mixed monolayer
#        ligand_fracs=['0.5','0.5']
        ligand_fracs=['0.5', '0.5']

        # number of rows/columns of ligands in SAM; determines box size; x-value should be even
        num_lig_x=8
        num_lig_y=8
        
        # single monolayer
        wantSingle = False
        
        # Defining layer separation
        num_layers = 1 # Number of layers of gold

        # size of z-dimension of water box (in nm) to add on top of SAM
        # Since each box added has same cross-sectional area, can control volume simply through ratio of the two z-dimensions.
        water_box_z=5.0 # nm of water in box
        methanol_box_z=0.0 # nm of methanol in box

        # Force field
        forcefield_folder = "opls-aa"

        # Output Directory & Files
        output_top="sam.top" # Topology file
        output_gro_name="sam.gro" # GRO File
        output_top_name="sam.top"
        output_folder=check_server_path(r'/Users/Ricardo/Documents/UW Madison/Van Lehn/Test')

        # Want Gold?
        wantGold=True # True if you want gold!
        wantVirtualSites=True # Virtual sites, True if you want
        wantPolarization=True # Polarization Sites, True if you want
        wantinterfaceff = False
        
    ### ON COMMAND LINE
    else: # Testing is off!
        # Adding options for command line input (e.g. --ligx, etc.)
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        
        #### DEFINING PARSER OPTIONS

        ### OUTPUT FILES / INPUT FILES
        ## OUTPUT GRO FILE
        parser.add_option("-g", "--ogro", dest="output_gro_name", action="store", type="string", help="Output GRO filename", default="double_sam.gro")
        ## OUTPUT TOP FILE
        parser.add_option("-t", "--otop", dest="output_top_name", action="store", type="string", help="Output TOP filename", default="double_sam.top")
        ## OUTPUT FOLDER
        parser.add_option("-o", "--ofold", dest="output_folder", action="store", type="string", help="Output folder", default=".")
        ## FORCE FIELD TYPE
        parser.add_option("-m", "--ff", dest="forcefield", action="store", type="string", help="Force field, typically a directory", default=".")
        ## PREPARATION FOLDER
        parser.add_option("-i", "--prep", dest="Prep_Folder", action="store", type="string", help="Preparation folder", default=".")

        ### LIGAND DETAILS
        ## LIGAND NAMES
        parser.add_option("-n", "--names", dest="ligand_names", action="callback", type="string", callback=get_ligand_args,
                  help="Name of ligand molecules to be loaded from ligands folder. For multiple ligands, separate each ligand name by comma (no whitespace)")
        ## LIGAND FRACTIONS
        parser.add_option("-f", "--fracs", dest="ligand_fracs", action="callback", type="string", callback=get_ligand_args,
                          help="Fraction of monolayer for each ligand specified by ligand_names. For multiple ligands, separate each ligand name by comma (no whitespace)")
        
        ### PLANAR PARSER OPTIONS
        ## NUMBER OF LIGANDS IN THE X AND Y DIRECTION
        parser.add_option("-x", "--ligx", dest="num_lig_x", action="store", type="int", help="Number of ligands in x-dimension on SAM", default="8")
        parser.add_option("-y", "--ligy", dest="num_lig_y", action="store", type="int", help="Number of ligands in y-dimension on SAM", default="7")
        ## LIGAND SEPARATION DISTANCE (nms)
        parser.add_option("--nlayers", dest="num_layers", action="store", type="int", help="Number of gold layers desired", default=None)
        ## GOLD FORCE FIELDS
        parser.add_option("-a", "--gold", dest="wantGold", action="store", type="string", help="True if you want gold", default=".")
        parser.add_option("-v", "--vir", dest="wantVirtualSites", action="store", type="string", help="True Or False, Want Virtual Sites?", default=".")
        parser.add_option("-p", "--pol", dest="wantPolarization", action="store", type="string", help="True Or False, Want Polarization (Rigid rod dipole)?", default=".")
        # Interface Force field
        parser.add_option("-w", "--int", dest="wantinterfaceff", action="store", type="string", help="True Or False, Want Interface Force Field Parameters?", default=".") 
        parser.add_option("-l", "--single", dest="wantSingle", action="store", type="string", help="True Or False, want single monolayer", default="False")
        parser.add_option("-P", "--pattern", dest="pattern", action="store", type="string", help="Destination to pattern file", default="None")
        
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        #### GETTING ARGUMENTS
        num_layers = options.num_layers
        ## OUTPUT / INPUT FILES
        output_gro_name = options.output_gro_name # Output gro file
        output_top_name = options.output_top_name # Output top file
        output_folder = options.output_folder # Output folder (for itps)
        prep_folder = options.Prep_Folder # Preparation folder
        forcefield_folder = options.forcefield # Force field
        
        ## LIGAND INFORMATION
        if (options.ligand_names): # Checking if ligand_name exists
            ligand_names = options.ligand_names
        else:
            print("ERROR! Must specify at least one ligand name with -n or --names")
            exit()
    
        if (options.ligand_fracs): # Checking if ligand fraction exists
            ligand_fracs = options.ligand_fracs
        else:
            print("ERROR! Must specify at least one ligand fraction with -f or --fracs")
            exit()
            
        #### CHECKING CONSISTENCY
        # ensure consistency of output for number of ligands and fractions specified
        if (len(ligand_fracs) != len(ligand_names)):
            print("ERROR: Different number of ligand names/fractions specified")
            exit()               
        # ensure fractions correctly sum
        frac_sum = 0.0
        for i in range(0, len(ligand_fracs)):
            frac_sum+=float(ligand_fracs[i])
        if (frac_sum > 1.01 or frac_sum < 0.99):
            print("ERROR: Ligand fractions do not sum to 1.0")
            exit()
        # Checking force fields
        possibleForceFields = [ 'opls-aa', 'charmm36-nov2016.ff', 'charmm36-jul2017.ff' ]
        if forcefield_folder not in possibleForceFields:
            print("Error! Force field selection is not one of the following: %s"%(str(possibleForceFields)))
            exit()
        
        # assign input parameters
        wantSingle=bool(options.wantSingle=="True") # True if you want single monolayer
        pattern=options.pattern                     # Desired pattern type (default: None, meaning random)
        num_lig_x = options.num_lig_x
        num_lig_y = options.num_lig_y
        
        #### EXTRACTING ADDITIONAL ARGUMENTS
        
        wantGold=bool(options.wantGold=="True") # True if you want gold!
        wantVirtualSites=bool(options.wantVirtualSites=="True") # True if you want virtual sites
        wantPolarization=bool(options.wantPolarization=="True") # True if you want rigid rod dipoles
        wantinterfaceff=bool(options.wantinterfaceff=="True") # True if you want interface force field

        #### CHECKING INPUT CORRECTNESS
        # check output for correctness
        if num_lig_x % 2: # Checking if num_lig_x is an even number
            print("WARNING! num_lig_x should be even for proper treatment of PBCs")
        # Check gold
        if wantGold == False:
            if wantVirtualSites == True or wantPolarization == True:
                print("Error, how can there be no gold with virtual sites and polarization???")
                print("Stopping here!")

    ########################################
    #### MAIN SCRIPT FOR LIGAND BUILDER ####
    ########################################
    
    ### FINDING FORCE FIELD DETAILS            
    forcefield = FORCEFIELD(
                            forcefield_folder = forcefield_folder
                            )
    

    ## PRINTING
    print("Ligand types:")
    print(', '.join(ligand_names))
    print("Ligand fractions:")
    print(', '.join(ligand_fracs))
    
    ################################
    ### CREATING LIGAND CLASSES ####
    ################################
    # set number of ligands based on input
    num_ligand_types = len(ligand_names)
    ### LOADING LIGANDS AND FRAGMENTS
    # list of ligands to be loaded
    ligands = []
    # load in all ligands used; output corresponding ITP files too.
    for cur_lig in ligand_names: # Uses Ligand class
        if cur_lig == 'none':
            ligands.append( None )
        else:
            print('Working on ligand:', cur_lig)
            ligands.append(Ligand(prep_ligand_folder=prep_folder, ligand_name = cur_lig))
    
    ## PRINTING
    if not wantSingle:
        print("Building double planar monolayer with the ligands")
    else:
        print("Building single planar monolayer with the ligands")
        
    print(" SAM size: %d by %d"%(num_lig_x, num_lig_y))   
    
    #%%
    ### INTRODUCTION OF GOLD
    if wantGold == True:
        ## Currently hard coded for one single monolayer -- still working on it!
        print("*** BUILDING GOLD LATTICE ***")
    
        # Now inserting gold atoms
        from MDBuilder.builder.make_gold_planar_111 import au_111_expansion_itp_CHARMM_GolP #imports function for expanding 111 facet
        
        ## FINDING NUMBER OF REPEATS (Hardcoded by math)
        repeat_x = 3 + int( 3/2.*(num_lig_x-2)) - 1
        repeat_y = num_lig_y - 1
        # repeat_z = 0.5 + (1.5) * (num_layers - 1)
        
        repeat_z = num_layers - 1
        
        ### PRINTING ITP FILE FOR GOLD
        gold_resname, goldCoordinates, atomname_list = au_111_expansion_itp_CHARMM_GolP( itp_file = GOLDITPNAME, 
                                                                                         repeat_x = repeat_x,
                                                                                         repeat_y = repeat_y,
                                                                                         repeat_z = repeat_z,
                                                                                         output_folder= output_folder,
                                                                                         wantVirtualSites=wantVirtualSites,
                                                                                         wantPolarization=wantPolarization,
                                                                                         forcefield = forcefield_folder,
                                                                                         wantinterfaceff = wantinterfaceff)
        
        ## FINDING THICKNESS OF THE MONOLAYER
        gold_geom_z_min = np.amin(goldCoordinates, axis=0)[2] # finds minimum of each column and outputs z-minimum
        gold_geom_z_max = np.amax(goldCoordinates, axis=0)[2] # finds maximum
        gold_thickness = gold_geom_z_max - gold_geom_z_min
        print("Gold thickness: %.3f nm"%(gold_thickness))
        
    
    #%%
    ## FINDING LAYER SEPARATION
    if wantGold == True:
        layer_separation = gold_thickness + 2 * GOLD_SULFUR_CUTOFF
    else:
        layer_separation = 0.4709 +  2 * GOLD_SULFUR_CUTOFF
    ## BOTTOM MONOLAYER --- GOLD --- TOP MONOLAYER
    ## DISTANCE OF GOLD-SULFUR MUST BE ACCOUNTED TWICE
    print("Layer separation between monolayers: %.3f nm"%(layer_separation))
        
    ### FIND THE TOTAL NUMBER OF LIGANDS THAT WILL BE ADDED
    # calculate number of ligands of each type
    tot_num_ligs = num_lig_x*num_lig_y
    ligand_nums = []
    # count number of each ligand
    for i in range(0, len(ligand_fracs)):
        ligand_nums.append(round(tot_num_ligs * float(ligand_fracs[i])))
        print("  Adding %d %s ligands to monolayers"%(round(tot_num_ligs * float(ligand_fracs[i])), ligand_names[i]))

    ### CREATING TWO MONOLAYERS
    top_monolayer = Monolayer(num_lig_x, num_lig_y, 1, layer_separation, ligands, ligand_nums, pattern) # Creates class for top monolayer
    bottom_monolayer = Monolayer(num_lig_x, num_lig_y, -1, 0.0, ligands, ligand_nums, pattern) # Creates class for bottom monolayer
    
    #%%
    ## CONCATENATE ALL MONOLAYER PROPERTIES TOGETHER
    total_atoms = top_monolayer.num_monolayer_atoms + bottom_monolayer.num_monolayer_atoms # sums all the atoms in the top and bottom monolayer
    # need to offset resids in bottom monolayer (arbitrarily chosen; could be top monolayer instead)
    top_resids = top_monolayer.monolayer_resids
    max_resid = np.amax(top_resids) # Maximum number of residues for the top
    bot_resids = bottom_monolayer.monolayer_resids + max_resid
    total_resids = np.append(top_resids, bot_resids) # Putting residue List together

    ## FIXING TOP LAYER GEOMETRY TO MATCH THAT OF GOLD
    offset_y = 0.5 * GOLD_DIAMETER_NM / np.sin(np.pi/3) #  Offsetting y because of crystal structure D/2 tan 60 * 1/2 <-- 
    # offset_y = 0.5*GOLD_DIAMETER_NM*np.tan(np.pi/6) # Offsetting y because of crystal structure D/2 tan 60
    top_monolayer.monolayer_geom = top_monolayer.monolayer_geom + np.array( [0,  offset_y , 0] ) #  + np.array( [offset_x, 0, 0] )
    
    
    ## FIXING BOTTOM LAYER GEOMETRY TO MATCH THAT OF GOLD
    # bottom_monolayer.monolayer_geom = bottom_monolayer.monolayer_geom - np.array( [0,  offset_y*2.0 , 0] ) #  + np.array( [offset_x, 0, 0] )
    
    ### GETTING TOTAL GEOMETRY AND OUTPUT NAMES
    total_geom = np.append(top_monolayer.monolayer_geom, bottom_monolayer.monolayer_geom, axis=0) # Adding top and  bottom monolayer residues
    total_outputnames = top_monolayer.monolayer_outputnames + bottom_monolayer.monolayer_outputnames # Adding top and bottom RESIDUE name and atom type

    ### FINDING BOX SIZE BASED ON GEOMETRY
    box_dimension = np.zeros(3, dtype='float')
    # set x/y dimensions based on number of ligands and presumed gold diameter
    box_dimension[0] = num_lig_x * 1.5 * GOLD_DIAMETER_NM
    box_dimension[1] = num_lig_y * GOLD_DIAMETER_NM * np.tan(np.pi/6)*1.5*2
    #For z-dimension, take min/max, and adjust geometry so all atoms are within box.
    geom_z_min = np.amin(total_geom, axis=0)[2] # finds minimum of each column and outputs z-minimum
    geom_z_max = np.amax(total_geom, axis=0)[2] # finds maximum
    # could pad with gold diameter, but no real reason to.
    box_dimension[2] = geom_z_max - geom_z_min
    # adjust all positions now
    total_geom = total_geom - [0.0, 0.0, geom_z_min]

    # goldThickness = 4.7091001410/10.0 # nm
    # Shifting coordinates
    if wantGold == True:
        shiftedGoldCoordinates = np.array(goldCoordinates[:]) - [0.0, 0.0, geom_z_min] + [0.0, 0.0, GOLD_SULFUR_CUTOFF] # Bringing gold up (Note, geom_z_min is generally negative) sulfur  bond, GOLD_DIAMETER_NM--offsetting  + GOLD_DIAMETER_NM/4.0
        totalGoldAtoms = len(shiftedGoldCoordinates)
    
        # Adding to total atoms
        total_atoms = total_atoms +  totalGoldAtoms # Adding total gold atoms
    
        # Adding to total residues
        currentResidueNumber = total_resids[-1] + 1 # new residue number
        goldArray = np.array( [currentResidueNumber] * totalGoldAtoms )
        total_resids = np.append(total_resids,goldArray )
    
        # Adding to geometry
        total_geom = np.append(total_geom[:], shiftedGoldCoordinates[:], axis=0)
    
        # Adding to output name
        GoldOutputName = []
        for currentGoldAtom in range(0, len(atomname_list)):
            GoldOutputName.append( "%5s%5s" %(gold_resname, str(atomname_list[currentGoldAtom]) ) )
        total_outputnames = np.append(total_outputnames[:], GoldOutputName )
    else:
        gold_resname=None
        
    ### PRINTING GRO / TOP FILE
    # output gro file with system geometry
    print_gro_file_combined_res_atom_names( output_gro = output_gro_name, 
                                             output_folder = output_folder,
                                             resids = total_resids,
                                             outputnames = total_outputnames,
                                             geom = total_geom,
                                             box_dimension = box_dimension,
                                             wantSingle = wantSingle )
    
    # Output topology file too
    Monolayer.print_top_file(forcefield, output_top_name, output_folder, num_lig_x, num_lig_y, ligand_names, ligands, ligand_nums, wantGold, GOLDITPNAME, gold_resname, wantSingle )
    
    ### PRINTING SUMMARY
    print("Done! Next, energy minimize, then add water to the output file.")
    print('')
    print(' **** FUTURE IMPROVEMENTS ****')
    print('  Need support for improper dihedrals (e.g. for arginine)')
    print("  Should add support for multiple force fields, different monolayer geometry, more exotic ligand architectures.")
    print('')
   
    
